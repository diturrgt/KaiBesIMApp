package com.kaibes.im.view.fragment;

import java.util.ArrayList;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;

import com.kaibes.im.R;
import com.kaibes.im.model.SocketWorker;
import com.kaibes.im.model.adapter.ContactinformationListviewAdapter;
import com.kaibes.im.model.data.AlertDialogData;
import com.kaibes.im.model.data.ContactListviewData;
import com.kaibes.im.model.hanlder.BaseHandler;
import com.kaibes.im.view.listview.ContactInformationListview;

public class ContactInformationMoreFragment extends KaibesFragment implements
	OnClickListener {

    private final static int REFRESH_INFORMATION = 0X1;

    private ContactInformationListview listview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
	setHandler(new BaseHandler<ContactInformationMoreFragment>(this) {

	    @Override
	    public void handleMessage(Message msg) {
		if (msg.what == REFRESH_INFORMATION) {
		    getData().listview.getBaseAdapter().notifyDataSetChanged();
		    getData().listview.setHeaderText(getData()
			    .getKaibesApplication().getCurentContact()
			    .getRemark());
		}
	    }
	});
	super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	View rootView = inflater.inflate(
		R.layout.fragment_contactinformation_2, container, false);

	listview = (ContactInformationListview) rootView
		.findViewById(R.id.lvContactinformation);

	ArrayList<ContactListviewData> contactListviewDatas = ContactListviewData
		.getArraylist(getKaibesApplication());

	listview.setHeaderAndFooter(contactListviewDatas.get(0));
	listview.setHeaderTextListener(this);
	listview.setFooterButtonListener(this);

	if (getKaibesApplication().getContactInformationData(
		getKaibesApplication().getCurentContact()
			.getContactname()) == null) {
	    listview.setFooterButtonVisibility(View.GONE);
	}

	// 删掉头数据
	contactListviewDatas.remove(0);
	listview.setAdapter(new ContactinformationListviewAdapter(
		getKaibesActivity(), contactListviewDatas));

	rootView.findViewById(R.id.tvContactinformationBack)
		.setOnClickListener(this);

	return rootView;
    }

    @Override
    public boolean onBackPressed() {
	getKaibesActivity().changeFragment(getLastFragmentAndSetNull());
	return false;
    }

    @Override
    public void onClick(View v) {
	switch (v.getId()) {
	case R.id.tvContactinformationBack:
	    onBackPressed();
	    break;
	case R.id.btContactinformationDelete: {
	    getKaibesApplication().sendData(
		    1,
		    SocketWorker.ACTION_REMOVE_CONTACTS,
		    getKaibesApplication().getCurentContact()
			    .getContactname());
	    getKaibesApplication().removeContactFragmentData();
	    getKaibesActivity().changeFragment(MainFragment.class);
	    break;
	}
	case R.id.tvContactinformationContent: {
	    final EditText inputServer = new EditText(getKaibesActivity());

	    getKaibesActivity().getAlertDialogData().reset()
		    .setTitle("请输入好友备注").setStyle(AlertDialogData.STYLE_VIEW)
		    .setView(inputServer).setNegativeString("取消")
		    .setPositiveString("确定")
		    .setPositiveListener(new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			    String remark = inputServer.getText().toString();
			    listview.setHeaderText(remark);
			    getKaibesApplication().getCurentContact()
				    .setRemark(remark);
			    getKaibesApplication().sendData(
				    1,
				    SocketWorker.ACTION_CHANGE_REMARK,
				    getKaibesApplication()
					    .getCurentContact()
					    .getContactname(), remark);
			}
		    });
	    getKaibesActivity().showAlertDialog();
	    break;
	}

	default:
	    break;
	}
    }

    @Override
    protected void reset() {
	if (getKaibesApplication().getContactInformationData(
		getKaibesApplication().getCurentContact()
			.getContactname()) != null) {
	    listview.setFooterButtonVisibility(View.VISIBLE);
	} else {
	    listview.setFooterButtonVisibility(View.GONE);
	}
	refresh(REFRESH_INFORMATION);
    }

}
