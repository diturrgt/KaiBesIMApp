package com.kaibes.im.view.fragment;

import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.kaibes.im.R;
import com.kaibes.im.model.adapter.SearchResultListviewAdapter;
import com.kaibes.im.model.data.ContactInformationData;
import com.kaibes.im.model.hanlder.BaseHandler;
import com.kaibes.im.view.listview.SearchResultListview;

public class SearchResultFragment extends KaibesFragment {

    private SearchResultListview listview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	View rootView = inflater.inflate(R.layout.fragment_contactadd_result,
		container, false);
	setHandler(new BaseHandler<SearchResultFragment>(this) {

	    @Override
	    public void handleMessage(Message msg) {
		reset();
	    }
	});
	listview = (SearchResultListview) rootView
		.findViewById(R.id.searchResultListview);
	listview.setAdapter(new SearchResultListviewAdapter(getKaibesActivity()));
	return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

	getView().findViewById(R.id.resultBack).setOnClickListener(
		new OnClickListener() {

		    @Override
		    public void onClick(View v) {
			onBackPressed();
		    }
		});
	listview.setOnItemClickListener(new OnItemClickListener() {

	    @Override
	    public void onItemClick(AdapterView<?> parent, View view,
		    int position, long id) {
		ContactInformationData contactInformationData = (ContactInformationData) parent
			.getAdapter().getItem(position);
		getKaibesApplication().setCurrentContact(
			contactInformationData);
		getKaibesActivity().changeFragment(
			ContactInformationAddFragment.class);
	    }
	});

	super.onActivityCreated(savedInstanceState);
    }

    @Override
    public boolean onBackPressed() {
	getKaibesActivity().changeFragment(getLastFragmentAndSetNull());
	return false;
    }

    @Override
    protected void reset() {
	if (listview != null && listview.getBaseAdapter() != null) {
	    listview.getBaseAdapter().notifyDataSetChanged();
	}
    }

}
