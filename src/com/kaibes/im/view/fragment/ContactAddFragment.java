package com.kaibes.im.view.fragment;

import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

import com.kaibes.im.R;
import com.kaibes.im.model.SocketWorker;
import com.kaibes.im.model.hanlder.BaseHandler;
import com.kaibes.im.view.CleanableEditeText;

public class ContactAddFragment extends KaibesFragment implements
	OnClickListener {

    private CleanableEditeText etAddcontactBykaibes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	View rootView = inflater.inflate(R.layout.fragment_contactadd,
		container, false);
	setHandler(new BaseHandler<KaibesFragment>(this) {

	    @Override
	    public void handleMessage(Message msg) {
		switch (msg.what) {
		case KaibesFragment.REFRESH_FRAGMENT:
		    // reset();
		    break;
		case 2:
		    // ((BaseAdapter) conversationListView.getAdapter())
		    // .notifyDataSetChanged();
		    break;
		case 3:
		    break;
		default:
		    break;
		}
	    }
	});
	return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
	initTabHost();

	getView().findViewById(R.id.tvAddcontactContact).setOnClickListener(
		new OnClickListener() {

		    @Override
		    public void onClick(View v) {
			onBackPressed();
		    }
		});

	// findcontact
	etAddcontactBykaibes = (CleanableEditeText) getView().findViewById(
		R.id.etAddcontactBykaibes);
	getView().findViewById(R.id.tvAddcontact).setOnClickListener(this);
	getView().findViewById(R.id.tvFindNeighbor).setOnClickListener(this);

	super.onActivityCreated(savedInstanceState);
    }

    private void initTabHost() {
	TabHost tabHost = (TabHost) getView().findViewById(R.id.mainAddcontact);
	tabHost.setup();
	tabHost.setOnTabChangedListener(new OnTabChangeListener() {

	    @Override
	    public void onTabChanged(String tabId) {
		if (tabId.equals("findContact")) {
		    refresh(1);
		}

		if (tabId.equals("findGroup")) {
		    refresh(2);
		}

		if (tabId.equals("findService")) {
		    refresh(3);
		}
	    }
	});

	TabSpec spec1 = tabHost.newTabSpec("findContact");
	TabSpec spec2 = tabHost.newTabSpec("findGroup");
	TabSpec spec3 = tabHost.newTabSpec("findService");

	spec1.setContent(R.id.tabFindContact);
	spec2.setContent(R.id.tabFindGroup);
	spec3.setContent(R.id.tabFindService);

	spec1.setIndicator(LayoutInflater.from(getActivity()).inflate(
		R.layout.tab_addcontact_widget_findcontact, null));
	spec2.setIndicator(LayoutInflater.from(getActivity()).inflate(
		R.layout.tab_addcontact_widget_findgroup, null));
	spec3.setIndicator(LayoutInflater.from(getActivity()).inflate(
		R.layout.tab_addcontact_widget_findservice, null));

	tabHost.addTab(spec1);
	tabHost.addTab(spec2);
	tabHost.addTab(spec3);
    }

    @Override
    public boolean onBackPressed() {
	getKaibesActivity().changeFragment(getLastFragmentAndSetNull());
	return false;
    }

    @Override
    protected void reset() {
	etAddcontactBykaibes.setText("");
    }

    @Override
    public void onClick(View v) {
	switch (v.getId()) {
	case R.id.tvAddcontactContact:
	    onBackPressed();
	    break;

	case R.id.tvAddcontact: {
	    String username = etAddcontactBykaibes.getText().toString();
	    if (username.equals(getKaibesApplication().getUsername())) {
		getKaibesActivity().getToastData().setMessage("不可以添加自己为好友！");
		getKaibesActivity().showToast();
	    } else if (getKaibesApplication().getContactInformationData(
		    username) != null) {
		getKaibesActivity().getToastData().setMessage("对方已经是您的好友！");
		getKaibesActivity().showToast();
	    } else {
		getKaibesActivity().getProgressDialogData().reset()
			.setTitle("搜索用户").setMessage("正在搜索用户。。。");
		getKaibesActivity().showProgressDialog();

		getKaibesActivity().getKaibesApplication().sendData(1,
			SocketWorker.ACTION_SEARCH,
			etAddcontactBykaibes.getText().toString(), "", "", "",
			"");
	    }
	    etAddcontactBykaibes.setText("");
	}
	    break;

	case R.id.tvFindNeighbor:
	    getKaibesActivity().getProgressDialogData().reset()
		    .setTitle("搜索用户").setMessage("正在获取在线用户。。。");
	    getKaibesActivity().showProgressDialog();

	    getKaibesApplication()
		    .sendData(1, SocketWorker.ACTION_USER_ONLINE);
	    break;

	default:
	    break;
	}
    }

}
