package com.kaibes.im.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kaibes.im.R;

public class PhotoShowFragment extends KaibesFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	View rootView = inflater.inflate(R.layout.fragment_photoshow,
		container, false);
	return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
	reset();
	super.onActivityCreated(savedInstanceState);
    }

    @Override
    public boolean onBackPressed() {
	getKaibesActivity().changeFragment(getLastFragmentAndSetNull());
	return false;
    }

    @Override
    protected void reset() {
	ImageView imageView = (ImageView) getView().findViewById(
		R.id.ivPhotoShow);
	imageView.setImageBitmap(getKaibesApplication().getPhotoshowBitmap());
    }

}
