package com.kaibes.im.view.fragment;

import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kaibes.im.R;
import com.kaibes.im.model.hanlder.BaseHandler;
import com.kaibes.im.view.RoundImageView;

public class ContactInformationFragment extends KaibesFragment implements
	OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
	setHandler(new BaseHandler<ContactInformationFragment>(this) {

	    @Override
	    public void handleMessage(Message msg) {
		switch (msg.what) {
		case 1:
		    reset();
		    break;

		default:
		    break;
		}
	    }
	});
	super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	View rootView = inflater.inflate(R.layout.fragment_contactinformation_1, container, false);

	rootView.findViewById(R.id.tvContactinformationMsg).setOnClickListener(this);
	rootView.findViewById(R.id.tvContactinformationBack).setOnClickListener(this);
	rootView.findViewById(R.id.tvContactinformationMore).setOnClickListener(this);
	rootView.findViewById(R.id.ivUserinformationPhoto).setOnClickListener(this);

	return rootView;
    }

    @Override
    public void onStart() {
	reset();
	super.onStart();
    }

    protected void reset() {
	getKaibesApplication()
		.setContactPhotoImage(
			(RoundImageView) getView().findViewById(
				R.id.ivUserinformationPhoto),
			getKaibesApplication().getCurentContact()
				.getPhoto());

	((TextView) getView().findViewById(R.id.tvContactinformationRemarks))
		.setText(getKaibesApplication().getCurentContact()
			.getRemark());
	((TextView) getView().findViewById(R.id.tvContactinformationNickname))
		.setText(getKaibesApplication().getCurentContact()
			.getNickname());
	((TextView) getView()
		.findViewById(R.id.tvContactinformationContactname))
		.setText(getKaibesApplication().getCurentContact()
			.getContactname());
	((TextView) getView()
		.findViewById(R.id.tvContactinformationInformation))
		.setText(getKaibesApplication().getCurentContact()
			.getInformation());
	((TextView) getView().findViewById(R.id.tvContactinformationSignature))
		.setText(getKaibesApplication().getCurentContact()
			.getSignature());
	((TextView) getView().findViewById(R.id.tvContactinformationEmail))
		.setText(getKaibesApplication().getCurentContact()
			.getEmail());
    }

    @Override
    public boolean onBackPressed() {
	getKaibesActivity().changeFragment(getLastFragmentAndSetNull());
	return false;
    }

    @Override
    public void onClick(View v) {
	switch (v.getId()) {
	case R.id.tvContactinformationMsg: {
	    if (!getKaibesApplication().getChatListviewDatas().contains(getKaibesApplication().getCurentContact().getContactname())) {
		getKaibesApplication().addConversationFragmentData();
	    }
	    getKaibesActivity().changeFragment(ChatFragment.class);
	    break;
	}

	case R.id.tvContactinformationBack: {
	    getKaibesActivity().changeFragment(getLastFragmentAndSetNull());
	}
	    break;

	case R.id.tvContactinformationMore: {
	    getKaibesActivity().changeFragment(
		    ContactInformationMoreFragment.class);
	}
	    break;
	    
	case R.id.ivUserinformationPhoto:{
	    getKaibesApplication().setPhotoshowBitmap(((RoundImageView)getView().findViewById(R.id.ivUserinformationPhoto)).getBitmap());
	    getKaibesActivity().changeFragment(PhotoShowFragment.class);
	}
	    break;

	default:
	    break;
	}
    }

}
