package com.kaibes.im.view.fragment;

import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kaibes.im.R;
import com.kaibes.im.model.SocketWorker;
import com.kaibes.im.model.hanlder.BaseHandler;
import com.kaibes.im.view.RoundImageView;

public class ContactInformationAddFragment extends KaibesFragment implements
	OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
	setHandler(new BaseHandler<ContactInformationAddFragment>(this) {

	    @Override
	    public void handleMessage(Message msg) {
		switch (msg.what) {
		case 1:
		    reset();
		    break;

		default:
		    break;
		}
	    }
	});
	super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	View rootView = inflater.inflate(
		R.layout.fragment_contactinformation_add, container, false);
	rootView.findViewById(R.id.tvContactinformationAdd).setOnClickListener(
		this);
	rootView.findViewById(R.id.tvContactinformationBack)
		.setOnClickListener(this);
	rootView.findViewById(R.id.tvContactinformationMore)
		.setOnClickListener(this);
	return rootView;
    }

    @Override
    public void onStart() {
	reset();
	super.onStart();
    }

    @Override
    public boolean onBackPressed() {
	getKaibesActivity().changeFragment(getLastFragmentAndSetNull());
	return false;
    }

    @Override
    public void onClick(View v) {
	switch (v.getId()) {
	case R.id.tvContactinformationAdd: {
	    getKaibesApplication().sendData(
		    1,
		    SocketWorker.ACTION_ADD_CONTACTS,
		    getKaibesApplication().getUsername(),
		    getKaibesApplication().getCurentContact()
			    .getContactname());
	    getKaibesApplication().addTempSearchData(
		    getKaibesApplication().getCurentContact());
	    onBackPressed();
	    break;
	}

	case R.id.tvContactinformationBack:
	    onBackPressed();
	    break;

	case R.id.tvContactinformationMore:
	    getKaibesActivity().changeFragment(
		    ContactInformationMoreFragment.class);
	    break;

	default:
	    break;
	}
    }

    @Override
    protected void reset() {
	getKaibesApplication()
		.setSearchPhotoImage(
			(RoundImageView) getView().findViewById(
				R.id.ivUserinformationPhoto),
			getKaibesApplication().getCurentContact()
				.getPhoto());
	((TextView) getView().findViewById(R.id.tvContactinformationRemarks))
		.setText(getKaibesApplication().getCurentContact()
			.getRemark());
	((TextView) getView().findViewById(R.id.tvContactinformationNickname))
		.setText(getKaibesApplication().getCurentContact()
			.getNickname());
	((TextView) getView()
		.findViewById(R.id.tvContactinformationContactname))
		.setText(getKaibesApplication().getCurentContact()
			.getContactname());
	((TextView) getView()
		.findViewById(R.id.tvContactinformationInformation))
		.setText(getKaibesApplication().getCurentContact()
			.getInformation());
	((TextView) getView().findViewById(R.id.tvContactinformationSignature))
		.setText(getKaibesApplication().getCurentContact()
			.getSignature());
	((TextView) getView().findViewById(R.id.tvContactinformationEmail))
		.setText(getKaibesApplication().getCurentContact()
			.getEmail());
    }

}
