package com.kaibes.im.view.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;

import com.kaibes.im.KaibesApplication;
import com.kaibes.im.R;
import com.kaibes.im.control.activity.KaibesActivity;
import com.kaibes.im.model.hanlder.BaseHandler;

public abstract class KaibesFragment extends Fragment {

    public static final int REFRESH_FRAGMENT = 0x0;

    public static final String REGISTER_FRAGMENT = RegisterFragment.class
	    .getName();
    public static final String LOGIN_FRAGMENT = LoginFragment.class.getName();
    public static final String MAIN_FRAGMENT = MainFragment.class.getName();
    public static final String CHAT_FRAGMENT = ChatFragment.class.getName();
    public static final String CONTACT_ADD_FRAGMENT = ContactAddFragment.class
	    .getName();
    public static final String CONTACT_INFORMATION_FRAGMENT = ContactInformationFragment.class
	    .getName();
    public static final String CONTACT_INFORMATION_MORE_FRAGMENT = ContactInformationMoreFragment.class
	    .getName();
    public static final String CONTACT_INFORMATION_ADD_FRAGMENT = ContactInformationAddFragment.class
	    .getName();
    public static final String USER_INFORMATION_FRAGMENT = UserInformationFragment.class
	    .getName();
    public static final String USER_INFORMATION_EDIT_FRAGMENT = UserInformationEditFragment.class
	    .getName();
    public static final String PHOTO_SHOW_FRAGMENT = PhotoShowFragment.class
	    .getName();
    public static final String SEARCH_RESULT_FRAGMENT = SearchResultFragment.class
	    .getName();
    public static final String TEST_RESULT_FRAGMENT = AllOnlineFragment.class
	    .getName();

    private KaibesActivity kaibesActivity;
    private KaibesApplication kaibesimApplication;
    private Class<? extends KaibesFragment> lastFragment = null;

    private BaseHandler<? extends KaibesFragment> handler;

    @Override
    public void onAttach(Activity activity) {
	kaibesActivity = (KaibesActivity) activity;
	kaibesimApplication = getKaibesActivity().getKaibesApplication();
	super.onAttach(activity);
    }

    protected abstract void reset();

    public abstract boolean onBackPressed();

    public final void refresh(int i) {
	getHandler().sendMessage(getHandler().obtainMessage(i));
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
	if (!hidden) {
	    reset();
	}
	super.onHiddenChanged(hidden);
    }

    protected void exitApp() {
	getKaibesActivity()
		.getAlertDialogData()
		.reset()
		.setTitle(getString(R.string.sure))
		.setMessage(getString(R.string.isExit))
		.setPositiveString(getString(R.string.ok))
		.setPositiveListener(new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
			getKaibesApplication().stopService();
		    }
		}).setNegativeString(getString(R.string.cancel))
		.setNegativeListener(null);
	getKaibesActivity().showAlertDialog();
    }

    // ---------------------------GET AND SET 1--------------------------

    public KaibesActivity getKaibesActivity() {
	return kaibesActivity;
    }

    public Class<? extends KaibesFragment> getLastFragmentAndSetNull() {
	Class<? extends KaibesFragment> temp = lastFragment;
	setLastFragment(null);
	return temp;
    }

    public Class<? extends KaibesFragment> getLastFragment() {
	return lastFragment;
    }

    public void setLastFragment(Class<? extends KaibesFragment> lastFragment) {
	this.lastFragment = lastFragment;
    }

    public KaibesApplication getKaibesApplication() {
	return kaibesimApplication;
    }

    protected final BaseHandler<? extends KaibesFragment> getHandler() {
	return handler;
    }

    protected final void setHandler(
	    BaseHandler<? extends KaibesFragment> handler) {
	this.handler = handler;
    }

}
