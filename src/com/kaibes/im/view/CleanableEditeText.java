package com.kaibes.im.view;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.EditText;

public class CleanableEditeText extends EditText {

    private Callback callback = null;
    private Drawable drawableLeft = null;
    private Drawable drawableRight = null;

    public CleanableEditeText(Context context, AttributeSet attrs,
	    int defStyleAttr) {
	super(context, attrs, defStyleAttr);
	init();
    }

    public CleanableEditeText(Context context) {
	super(context);
	init();
    }

    public CleanableEditeText(Context context, AttributeSet attrs) {
	super(context, attrs);
	init();
    }

    private void init() {
	drawableLeft = getCompoundDrawables()[0];
	drawableRight = getCompoundDrawables()[2];
	setCallback(new Callback() {

	    @Override
	    public void onTextChanged(CharSequence s, int start, int before,
		    int count) {
		// TODO Auto-generated method stub

	    }

	    @Override
	    public void beforeTextChanged(CharSequence s, int start, int count,
		    int after) {
		// TODO Auto-generated method stub

	    }

	    @Override
	    public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub

	    }
	});

	addTextChangedListener(new TextWatcher() {

	    @Override
	    public void onTextChanged(CharSequence s, int start, int before,
		    int count) {
		callback.onTextChanged(s, start, before, count);

	    }

	    @Override
	    public void beforeTextChanged(CharSequence s, int start, int count,
		    int after) {
		callback.beforeTextChanged(s, start, count, after);
	    }

	    @Override
	    public void afterTextChanged(Editable s) {
		setDrawable();
		callback.afterTextChanged(s);
	    }
	});
	setCompoundDrawables(drawableLeft, null, null, null);
    }

    private void setDrawable() {
	if (length() == 0) {
	    setCompoundDrawables(drawableLeft, null, null, null);
	} else {
	    setCompoundDrawables(drawableLeft, null, drawableRight, null);
	}
    }

    @Override
    public boolean performClick() {
	// TODO Auto-generated method stub
	return super.performClick();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
	if (event.getAction() == MotionEvent.ACTION_UP) {
	    boolean isInnerWidth = (event.getX() > (getWidth() - getTotalPaddingRight()) && event
		    .getX() < (getWidth() - getPaddingRight()));

	    Rect rect = drawableRight.getBounds();
	    int height = rect.height();
	    int distance = (getHeight() - height) / 2;
	    boolean isInnerHeight = (event.getY() > distance)
		    && (event.getY() < (distance + height));

	    if (isInnerWidth && isInnerHeight) {
		setText("");
	    }

	}
	performClick();
	return super.onTouchEvent(event);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction,
	    Rect previouslyFocusedRect) {
	if (focused) {
	    setDrawable();
	} else {
	    setCompoundDrawables(drawableLeft, null, null, null);
	}
	super.onFocusChanged(focused, direction, previouslyFocusedRect);
    }

    public interface Callback {
	public void onTextChanged(CharSequence s, int start, int before,
		int count);

	public void beforeTextChanged(CharSequence s, int start, int count,
		int after);

	public void afterTextChanged(Editable s);
    }

    // =================================

    public void setCallback(Callback callback) {
	this.callback = callback;
    }

}
