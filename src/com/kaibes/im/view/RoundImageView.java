package com.kaibes.im.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.kaibes.im.utils.BitmapUtil;

public class RoundImageView extends ImageView {

    private Bitmap bitmap = null;

    public RoundImageView(Context context, AttributeSet attrs, int defStyleAttr) {
	super(context, attrs, defStyleAttr);
	// TODO Auto-generated constructor stub
    }

    public RoundImageView(Context context, AttributeSet attrs) {
	super(context, attrs);
	// TODO Auto-generated constructor stub
    }

    public RoundImageView(Context context) {
	super(context);
	// TODO Auto-generated constructor stub
    }

    public void setImageBitmapGray(Bitmap bm) {
	this.bitmap = bm;
	super.setImageBitmap(BitmapUtil.toRoundBitmapGray(getBitmap()));
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
	this.bitmap = bm;
	super.setImageBitmap(BitmapUtil.toRoundBitmap(getBitmap()));
    }

    public Bitmap getBitmap() {
	return bitmap;
    }

}
