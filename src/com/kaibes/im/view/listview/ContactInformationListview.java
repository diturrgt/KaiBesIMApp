package com.kaibes.im.view.listview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.kaibes.im.R;
import com.kaibes.im.model.adapter.ContactinformationListviewAdapter;
import com.kaibes.im.model.data.ContactListviewData;

public class ContactInformationListview extends ListView {

    private TextView headerTextView;
    private Button footerButton;

    private ContactinformationListviewAdapter baseAdapter;

    public ContactInformationListview(Context context, AttributeSet attrs,
	    int defStyleAttr) {
	super(context, attrs, defStyleAttr);
    }

    public ContactInformationListview(Context context, AttributeSet attrs) {
	super(context, attrs);
    }

    public ContactInformationListview(Context context) {
	super(context);
    }

    public void setHeaderAndFooter(ContactListviewData contactListviewData) {
	View header = LayoutInflater.from(getContext()).inflate(
		R.layout.list_item_contactinformation, this, false);
	View footer = LayoutInflater.from(getContext()).inflate(
		R.layout.list_footer_contactinformation, this, false);

	header.setPadding(0, 0, 0, 50);

	headerTextView = ((TextView) header
		.findViewById(R.id.tvContactinformationContent));
	footerButton = (Button) footer
		.findViewById(R.id.btContactinformationDelete);

	((TextView) header.findViewById(R.id.tvContactinformationName))
		.setText(contactListviewData.getName());
	headerTextView.setText(contactListviewData.getContent());

	addHeaderView(header);
	addFooterView(footer);

    }

    public void setHeaderText(String text) {
	headerTextView.setText(text);
    }

    public void setFooterButtonVisibility(int i) {
	footerButton.setVisibility(i);
    }

    public void setHeaderTextListener(OnClickListener onClickListener) {
	headerTextView.setOnClickListener(onClickListener);
    }

    public void setFooterButtonListener(OnClickListener onClickListener) {
	footerButton.setOnClickListener(onClickListener);
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
	baseAdapter = (ContactinformationListviewAdapter) adapter;
	super.setAdapter(adapter);
    }

    public ContactinformationListviewAdapter getBaseAdapter() {
	return baseAdapter;
    }
}
