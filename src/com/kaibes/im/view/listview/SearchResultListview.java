package com.kaibes.im.view.listview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class SearchResultListview extends ListView {

    private BaseAdapter baseAdapter;

    public SearchResultListview(Context context, AttributeSet attrs,
	    int defStyleAttr) {
	super(context, attrs, defStyleAttr);
    }

    public SearchResultListview(Context context, AttributeSet attrs) {
	super(context, attrs);
    }

    public SearchResultListview(Context context) {
	super(context);
    }

    public BaseAdapter getBaseAdapter() {
	return baseAdapter;
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
	this.baseAdapter = (BaseAdapter) adapter;
	super.setAdapter(adapter);
    }

}
