package com.kaibes.im.view.listview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.kaibes.im.R;

public class ContactListview extends ListView {

    private BaseAdapter baseAdapter = null;

    public ContactListview(Context context, AttributeSet attrs, int defStyleAttr) {
	super(context, attrs, defStyleAttr);
	init();
    }

    public ContactListview(Context context, AttributeSet attrs) {
	super(context, attrs);
	init();
    }

    public ContactListview(Context context) {
	super(context);
	init();
    }

    @SuppressLint("InflateParams")
    private void init() {
	addHeaderView(LayoutInflater.from(getContext()).inflate(
		R.layout.list_header_contact, null));
    }

    public BaseAdapter getBaseAdapter() {
	return baseAdapter;
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
	this.baseAdapter = (BaseAdapter) adapter;
	super.setAdapter(adapter);
    }

}
