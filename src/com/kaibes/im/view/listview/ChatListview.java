package com.kaibes.im.view.listview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class ChatListview extends ListView {

    public ChatListview(Context context, AttributeSet attrs, int defStyleAttr) {
	super(context, attrs, defStyleAttr);
    }

    public ChatListview(Context context, AttributeSet attrs) {
	super(context, attrs);
    }

    public ChatListview(Context context) {
	super(context);
    }

}
