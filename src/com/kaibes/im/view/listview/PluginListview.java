package com.kaibes.im.view.listview;

import com.kaibes.im.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class PluginListview extends ListView {

    private BaseAdapter baseAdapter = null;

    public PluginListview(Context context, AttributeSet attrs, int defStyleAttr) {
	super(context, attrs, defStyleAttr);
    }

    public PluginListview(Context context, AttributeSet attrs) {
	super(context, attrs);
    }

    public PluginListview(Context context) {
	super(context);
    }

    @SuppressLint("InflateParams")
    private void init() {
	addHeaderView(LayoutInflater.from(getContext()).inflate(
		R.layout.list_header_plugin, null));
    }

    public BaseAdapter getBaseAdapter() {
	return baseAdapter;
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
	init();
	this.baseAdapter = (BaseAdapter) adapter;
	super.setAdapter(adapter);
    }
}
