package com.kaibes.im.view.listview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class OnlineListview extends ListView {

    public OnlineListview(Context context, AttributeSet attrs, int defStyleAttr) {
	super(context, attrs, defStyleAttr);
    }

    public OnlineListview(Context context, AttributeSet attrs) {
	super(context, attrs);
    }

    public OnlineListview(Context context) {
	super(context);
    }
}
