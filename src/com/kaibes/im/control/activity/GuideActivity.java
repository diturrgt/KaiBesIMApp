package com.kaibes.im.control.activity;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.kaibes.im.R;
import com.kaibes.im.model.adapter.GuideViewpagerAdapter;

public class GuideActivity extends BaseActivity implements OnPageChangeListener {

    private ViewPager viewPager;
    private GuideViewpagerAdapter viewPagerAdapter;
    private List<View> views;
    private ImageView[] points;
    private int[] ids = { R.id.iv_guide_1, R.id.iv_guide_2, R.id.iv_guide_3,
	    R.id.iv_guide_4 };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	setContentView(R.layout.activity_guide);
	initViews();
	initGuidePoints();
	super.onCreate(savedInstanceState);
    }

    @SuppressLint("InflateParams")
    private void initViews() {
	LayoutInflater inflater = LayoutInflater.from(this);

	views = new ArrayList<View>();
	views.add(inflater.inflate(R.layout.viewpager_guide_1, null));
	views.add(inflater.inflate(R.layout.viewpager_guide_2, null));
	views.add(inflater.inflate(R.layout.viewpager_guide_3, null));
	views.add(inflater.inflate(R.layout.viewpager_guide_4, null));

	viewPagerAdapter = new GuideViewpagerAdapter(views, this);
	viewPager = (ViewPager) findViewById(R.id.viewpager_guide);
	viewPager.setAdapter(viewPagerAdapter);
	viewPager.setOnPageChangeListener(this);

	views.get(views.size() - 1).setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View v) {
		startActivity(new Intent(getApplicationContext(),
			LoginActivity.class));
		finish();
	    }
	});

    }

    private void initGuidePoints() {
	points = new ImageView[views.size()];
	for (int i = 0; i < views.size(); i++) {
	    points[i] = (ImageView) findViewById(ids[i]);
	}
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override
    public void onPageSelected(int arg0) {
	for (int i = 0; i < ids.length; i++) {
	    if (arg0 == i) {
		points[i].setImageResource(R.drawable.guide_jqn);
	    } else {
		points[i].setImageResource(R.drawable.guide_jqm);
	    }
	}
    }
}
