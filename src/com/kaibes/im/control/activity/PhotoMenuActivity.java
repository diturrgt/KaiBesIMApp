package com.kaibes.im.control.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.kaibes.im.R;

public class PhotoMenuActivity extends BaseActivity implements OnClickListener {
    public static final int REQUEST_CODE = 1;

    public static final int RESULT_CODE_DEFAULT = 0;
    public static final int RESULT_CODE_CANCEL = 1;
    public static final int RESULT_CODE_SET = 2;
    public static final int RESULT_CODE_SHOW = 3;
    public static final int RESULT_CODE_CHANGE = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	setContentView(R.layout.activity_menuphoto);
	findViewById(R.id.btnPhotomenuCancel).setOnClickListener(this);
	findViewById(R.id.btnPhotomenuSet).setOnClickListener(this);
	findViewById(R.id.btnPhotomenuShow).setOnClickListener(this);
	findViewById(R.id.btnPhotomenuChange).setOnClickListener(this);

	super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
	setResult(RESULT_CODE_DEFAULT);
	finish();
    }

    @Override
    public void onClick(View v) {
	switch (v.getId()) {
	case R.id.btnPhotomenuCancel:
	    setResult(RESULT_CODE_CANCEL);
	    break;

	case R.id.btnPhotomenuSet:
	    setResult(RESULT_CODE_SET);
	    break;

	case R.id.btnPhotomenuShow:
	    setResult(RESULT_CODE_SHOW);
	    break;

	case R.id.btnPhotomenuChange:
	    setResult(RESULT_CODE_CHANGE);
	    break;

	default:
	    break;
	}
	finish();
    }
}
