package com.kaibes.im.control.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.kaibes.im.R;

public class WelcomeActivity extends BaseActivity {

    private final static long TIME = 2000;
    private final static int GO_GUIDE = 1001;
    private final static int GO_LOGIN = 1002;

    private boolean isFirst;
    private SharedPreferences sharedPreferences;

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
	public void handleMessage(Message msg) {
	    switch (msg.what) {
	    case GO_GUIDE:
		goGuide();
		break;
	    case GO_LOGIN:
		goLogin();
		break;

	    default:
		break;
	    }
	};
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	setContentView(R.layout.activity_welcome);
	init();

	if (isFirst) {
	    handler.sendEmptyMessageDelayed(GO_GUIDE, TIME);
	    Editor editor = sharedPreferences.edit();
	    editor.putBoolean("isFirt", false);
	    editor.commit();
	} else {
	    handler.sendEmptyMessageDelayed(GO_LOGIN, TIME);
	}
	super.onCreate(savedInstanceState);
    }

    private void init() {
	sharedPreferences = getPreferences(Context.MODE_APPEND);
	isFirst = sharedPreferences.getBoolean("isFirt", true);
    }

    /**
     * 
     * @author QiuShuiCai
     * @email 414100905@qq.com
     * @date 2015-1-20 下午4:54:29
     * @description 前往登陆界面
     * @return void
     * 
     */
    protected void goLogin() {
	startActivity(new Intent(this, LoginActivity.class));
	finish();
    }

    /**
     * 
     * @author QiuShuiCai
     * @email 414100905@qq.com
     * @date 2015-1-20 下午4:55:31
     * @description 前往引导界面
     * @return void
     * 
     */
    private void goGuide() {
	startActivity(new Intent(this, GuideActivity.class));
	finish();
    }
}
