package com.kaibes.im.control.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import com.kaibes.im.KaibesApplication;
import com.kaibes.im.model.SocketWorker;
import com.kaibes.im.model.data.AlertDialogData;
import com.kaibes.im.model.data.ProgressDialogData;
import com.kaibes.im.model.data.ToastData;
import com.kaibes.im.model.hanlder.BaseHandler;
import com.kaibes.im.model.hanlder.KaibesHandler;
import com.kaibes.im.view.fragment.KaibesFragment;

public abstract class KaibesActivity extends BaseActivity {

    private KaibesApplication kaibesimApplication;
    private Class<? extends KaibesFragment> currentFragment = null;

    private BaseHandler<KaibesActivity> handler = null;
    private ToastData toastData = null;
    private AlertDialogData alertDialogData = null;
    private ProgressDialogData progressDialogData;

    private ProgressDialog progressDialog = null;

    private int containerViewId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	kaibesimApplication = (KaibesApplication) getApplication();
	getKaibesApplication().setCurrentActivity(this);

	toastData = new ToastData();
	alertDialogData = new AlertDialogData();
	progressDialogData = new ProgressDialogData();
	handler = new KaibesHandler(this);

	super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
	if (((KaibesFragment) getFragmentManager().findFragmentByTag(
		currentFragment.getName())).onBackPressed()) {
	    super.onBackPressed();
	}
    }

    /**
     * 刷新fragment界面数据的方法
     * 
     * @param fragmentName
     *            目标fragment的名字
     * @param i
     *            刷新内容的标志，由具体方法决定它的意义
     * @author QiuShuiCai
     */
    public void refresh(String fragmentName, int i) {
	KaibesFragment kaibesFragment = null;
	if ((kaibesFragment = ((KaibesFragment) getFragmentManager()
		.findFragmentByTag(fragmentName))) != null) {
	    kaibesFragment.refresh(i);
	}
    }

    /**
     * 查找指定的fragment
     * 
     * @param fragmentName
     *            目标fragment的名字
     * @author QiuShuiCai
     * @return KaibesFragment
     */
    public KaibesFragment findFragmentByTag(String fragmentName) {
	return (KaibesFragment) getFragmentManager().findFragmentByTag(
		fragmentName);
    }

    /**
     * 隐藏指定的fragment
     * 
     * @param fragmentName
     *            目标fragment的名字
     * @author QiuShuiCai
     */
    public void hideFragment(String fragmentName) {
	getFragmentManager().beginTransaction()
		.hide(findFragmentByTag(fragmentName)).commit();
    }

    /**
     * 隐藏指定的fragment，并展示另一个fragment
     * 
     * @param hideFragment
     *            隐藏目标fragment的名字
     * @param showFragment
     *            展示目标fragment的名字
     * @author QiuShuiCai
     */
    public void hideAndShowFragment(String hideFragment, String showFragment) {
	getFragmentManager().beginTransaction()
		.hide(findFragmentByTag(hideFragment))
		.show(findFragmentByTag(showFragment)).commit();
	// setCurrentFragment(showFragment);
    }

    /**
     * 隐藏当前的fragment，并展示另一个fragment
     * 
     * @param showFragment
     *            展示目标fragment的名字
     * @author QiuShuiCai
     */
    public void hideAndShowFragment(String showFragment) {
	hideAndShowFragment(getCurrentFragment().getName(), showFragment);
    }

    /**
     * 退出登录并跳转到登陆界面
     * 
     * @author QiuShuiCai
     */
    public void back2Login() {
	getKaibesApplication().reset();
	Intent intent = new Intent(this, LoginActivity.class);
	intent.putExtra("isBack", true);
	startActivity(intent);
	finish();
    }

    /**
     * @param
     * @author QiuShuiCai
     * @return void
     */
    public void try2Login() {
	getKaibesApplication().setLoginAgain(true);
	getKaibesApplication().sendData(1, SocketWorker.ACTION_LOGIN,
		getKaibesApplication().getUsername(),
		getKaibesApplication().getPassword());
    }

    /**
     * 切换fragment的方法
     * 
     * @param fragmentName
     *            fragment的名字
     * @author QiuShuiCai
     */
    public void changeFragment(Class<? extends KaibesFragment> FragmentClass) {
	KaibesFragment kaibesFragment = null;
	if ((kaibesFragment = (KaibesFragment) getFragmentManager()
		.findFragmentByTag(FragmentClass.getName())) == null) {
	    try {
		kaibesFragment = FragmentClass.newInstance();
	    } catch (InstantiationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    } catch (IllegalAccessException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	    if (getCurrentFragment() == null) {
		getFragmentManager()
			.beginTransaction()
			.add(getContainerViewId(), kaibesFragment,
				FragmentClass.getName()).commit();
	    } else {
		getFragmentManager()
			.beginTransaction()
			.hide(getFragmentManager().findFragmentByTag(
				getCurrentFragment().getName()))
			.add(getContainerViewId(), kaibesFragment,
				FragmentClass.getName()).commit();
	    }
	} else if (kaibesFragment.isHidden()) {
	    hideAndShowFragment(FragmentClass.getName());
	}

	if (kaibesFragment != null && kaibesFragment.getLastFragment() == null) {
	    kaibesFragment.setLastFragment(getCurrentFragment());
	}

	setCurrentFragment(FragmentClass);
    }

    /**
     * 向用户展示一个ProgressDialog
     * 
     * @param
     * @author QiuShuiCai
     */
    public void showProgressDialog() {
	show(KaibesHandler.SHOW_PROGRESS_DIALOG);
    }

    /**
     * 关闭当前activity里得ProgressDialog
     * 
     * @param
     * @author QiuShuiCai
     */
    public void closeProgressDialog() {
	show(KaibesHandler.CLOSE_PROGRESS_DIALOG);
    }

    /**
     * 向用户展示一个AlertDialog
     * 
     * @author QiuShuiCai
     */
    public void showAlertDialog() {
	show(KaibesHandler.SHOW_ALERT_DIALOG);
    }

    /**
     * 向用户展示一个Toast
     * 
     * @author QiuShuiCai
     */
    public void showToast() {
	show(KaibesHandler.SHOW_TOAST);
    }

    /**
     * @param i
     *            Handler的目标
     * @author QiuShuiCai
     */
    private void show(int i) {
	getHandler().sendMessage(getHandler().obtainMessage(i));
    }

    public void startActivityAndFinish(Class<?> cls) {
	closeProgressDialog();
	startActivity(cls);
	finish();
    }

    private void startActivity(Class<?> cls) {
	startActivity(new Intent(this, cls));
    }

    // ---------------------------GET AND SET 1--------------------------

    // KaibesApplication
    public KaibesApplication getKaibesApplication() {
	return kaibesimApplication;
    }

    // CurrentFragment
    public Class<? extends KaibesFragment> getCurrentFragment() {
	return currentFragment;
    }

    public void setCurrentFragment(
	    Class<? extends KaibesFragment> currentFragment) {
	this.currentFragment = currentFragment;
    }

    public BaseHandler<KaibesActivity> getHandler() {
	return handler;
    }

    public void setHandler(BaseHandler<KaibesActivity> handler) {
	this.handler = handler;
    }

    public ToastData getToastData() {
	return toastData;
    }

    public AlertDialogData getAlertDialogData() {
	return alertDialogData;
    }

    public ProgressDialog getProgressDialog() {
	return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
	this.progressDialog = progressDialog;
    }

    public int getContainerViewId() {
	return containerViewId;
    }

    public void setContainerViewId(int containerViewId) {
	this.containerViewId = containerViewId;
    }

    public ProgressDialogData getProgressDialogData() {
	return progressDialogData;
    }

}
