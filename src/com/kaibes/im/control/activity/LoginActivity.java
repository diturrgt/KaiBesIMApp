package com.kaibes.im.control.activity;

import android.os.Bundle;

import com.kaibes.im.R;
import com.kaibes.im.view.fragment.KaibesFragment;
import com.kaibes.im.view.fragment.LoginFragment;

public class LoginActivity extends KaibesActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	setContentView(R.layout.activity_login);
	setContainerViewId(R.id.container);
	changeFragment(LoginFragment.class);
	super.onCreate(savedInstanceState);

	if (savedInstanceState != null
		&& savedInstanceState.getBoolean("iskilled")) {
	    getKaibesApplication().doWroks();
	}
    }

    @Override
    protected void onRestart() {
	getKaibesApplication().doWroks();
	refresh(getCurrentFragment().getName(), KaibesFragment.REFRESH_FRAGMENT);
	super.onRestart();
    }

    @Override
    protected void onResume() {
	if (!getIntent().getBooleanExtra("isBack", false)) {
	    getKaibesApplication().startService();
	}
	super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
	outState.putBoolean("iskilled", true);
	super.onSaveInstanceState(outState);
    }

}
