package com.kaibes.im.control.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;

import com.kaibes.im.R;
import com.kaibes.im.model.KaibesDrawerListener;
import com.kaibes.im.model.SocketWorker;
import com.kaibes.im.model.data.PluginListviewData;
import com.kaibes.im.view.fragment.ChatFragment;
import com.kaibes.im.view.fragment.ContactAddFragment;
import com.kaibes.im.view.fragment.KaibesFragment;
import com.kaibes.im.view.fragment.MainFragment;
import com.kaibes.im.view.fragment.UserInformationFragment;

public class MainActivity extends KaibesActivity implements OnClickListener {

    public static final int REQUEST_CODE_MAINMENU = 0x1;

    private boolean isNewIntent = false;

    /**
     * 这个是用来实现拖动菜单的组件，在V4里
     */
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	setContentView(R.layout.activity_main);
	setContainerViewId(R.id.container);

	super.onCreate(savedInstanceState);

	changeFragment(MainFragment.class);
	initialize();

	if (savedInstanceState != null
		&& savedInstanceState.getBoolean("iskilled")) {
	    getKaibesApplication().doWroks();
	}
    }

    @Override
    protected void onRestart() {
	if (isNewIntent) {
	    isNewIntent = false;
	} else {
	}

	getKaibesApplication().doWroks();
	refresh(getCurrentFragment().getName(), KaibesFragment.REFRESH_FRAGMENT);
	super.onRestart();
    }

    @Override
    protected void onNewIntent(Intent intent) {
	isNewIntent = true;

	switch (intent.getIntExtra("notification", 0x0)) {
	case MainFragment.NOTIFICATION_NOTIFICATION: {

	    PluginListviewData pluginListviewData = getKaibesApplication()
		    .getPluginListviewDatas().get(
			    getKaibesApplication().getPluginListviewDatas()
				    .size() - 1);
	    ((MainFragment) findFragmentByTag(KaibesFragment.MAIN_FRAGMENT))
		    .dealWithNotificationData(pluginListviewData);
	}
	    break;

	case MainFragment.NOTIFICATION_MESSAGE: {
	    String contactname = intent.getStringExtra("contactname");
	    getKaibesApplication().setCurentContact(contactname);

	    if (getCurrentFragment().equals(ChatFragment.class)) {
		getKaibesApplication().refresh(KaibesFragment.CHAT_FRAGMENT, 1);
	    } else {
		changeFragment(ChatFragment.class);
	    }
	}
	    break;

	default:
	    break;
	}
	super.onNewIntent(intent);
    }

    /**
     * 重写后退键功能，如果这时候菜单是打开的，后退键关闭菜单，否则执行父类的后退键功能
     */
    @Override
    public void onBackPressed() {
	if (drawerLayout.isDrawerOpen(Gravity.LEFT)
		|| drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
	    closeMenu();
	} else {
	    super.onBackPressed();
	}
    }

    @Override
    public void onClick(View v) {
	switch (v.getId()) {
	case R.id.mainMenuLeftPhoto:
	    closeMenu();
	    changeFragment(UserInformationFragment.class);
	    break;

	case R.id.mainMenuRightAdd:
	    closeMenu();
	    changeFragment(ContactAddFragment.class);
	    break;

	default:
	    break;
	}
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
	if (keyCode == KeyEvent.KEYCODE_MENU) {
	    startActivityForResult(new Intent(this, MainMenuActivity.class),
		    REQUEST_CODE_MAINMENU);
	    return true;
	}
	return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
	outState.putBoolean("iskilled", true);
	super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	if (requestCode == REQUEST_CODE_MAINMENU) {
	    if (resultCode == MainMenuActivity.RESULT_CODE_UNLOGIN) {
		getKaibesApplication().sendData(1,
			SocketWorker.ACTION_USER_UNLOGIN);
		back2Login();
	    }
	}
	super.onActivityResult(requestCode, resultCode, data);
    }

    private void initialize() {
	// 获取DrawerLayout
	drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
	// 关闭右边的手势滑动
	drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED,
		Gravity.RIGHT);
	// 设置透明度变化
	drawerLayout.setScrimColor(0xa0000000);
	// 设置监听器
	drawerLayout.setDrawerListener(new KaibesDrawerListener(drawerLayout,
		getKaibesApplication()));
	findViewById(R.id.mainMenuLeftPhoto).setOnClickListener(this);
	findViewById(R.id.mainMenuRightAdd).setOnClickListener(this);
    }

    /**
     * 这个方法可以开启左菜单，由布局里设置onClick调用
     * 
     * @param view
     *            布局里设置了onClick的view
     * @author QiuShuiCai
     */
    public void openLeftMenu(View view) {
	drawerLayout.openDrawer(Gravity.LEFT);
	drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED,
		Gravity.LEFT);
    }

    /**
     * 这个方法可以开启右菜单，由布局里设置onClick调用
     * 
     * @param view
     *            布局里设置了onClick的view
     * @author QiuShuiCai
     */
    public void openRightMenu(View view) {
	drawerLayout.openDrawer(Gravity.RIGHT);
	drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED,
		Gravity.RIGHT);
    }

    /**
     * 关闭菜单的方法
     * 
     * @author QiuShuiCai
     */
    public void closeMenu() {
	drawerLayout.closeDrawers();
    }

}
