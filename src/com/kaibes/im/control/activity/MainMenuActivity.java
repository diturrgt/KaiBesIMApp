package com.kaibes.im.control.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;

import com.kaibes.im.R;

public class MainMenuActivity extends BaseActivity implements OnClickListener {

    public static final int RESULT_CODE_UNLOGIN = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	setContentView(R.layout.activity_menumain);
	super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
	findViewById(R.id.btnMainmenuCancel).setOnClickListener(this);
	findViewById(R.id.btnMainmenuExit).setOnClickListener(this);
	findViewById(R.id.btnMainmenuHelp).setOnClickListener(this);
	findViewById(R.id.btnMainmenuUpdate).setOnClickListener(this);
	super.onStart();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
	if (keyCode == KeyEvent.KEYCODE_MENU) {
	    finish();
	    return true;
	}
	return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
	finish();
    }

    @Override
    public void onClick(View v) {
	switch (v.getId()) {
	case R.id.btnMainmenuCancel:
	    break;

	case R.id.btnMainmenuExit:
	    setResult(RESULT_CODE_UNLOGIN);
	    break;

	case R.id.btnMainmenuHelp:
	    // TODO 帮助与反馈
	    break;

	case R.id.btnMainmenuUpdate:
	    // TODO 部分更新下载
	    break;

	default:
	    break;
	}
	finish();
    }

}
