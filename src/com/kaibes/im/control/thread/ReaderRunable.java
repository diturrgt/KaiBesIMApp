package com.kaibes.im.control.thread;

import java.io.BufferedReader;
import java.io.IOException;

import com.kaibes.im.model.SocketWorker;

public class ReaderRunable implements Runnable {

    private BufferedReader bfReader;
    private String json;

    public ReaderRunable(BufferedReader bfReader) {
	this.bfReader = bfReader;
    }

    @Override
    public void run() {
	boolean flag = true;

	while (flag) {
	    try {
		if (setJson(bfReader.readLine())) {
		    SocketWorker.getInstance().callBack(json);
		} else {
		    flag = false;
		}
	    } catch (IOException e) {
		flag = false;
		// e.printStackTrace();
	    }
	}
    }

    // ---------------------------GET AND SET 1--------------------------
    private boolean setJson(String json) {
	if (json != null) {
	    this.json = json;
	    return true;
	} else {
	    return false;
	}
    }

}
