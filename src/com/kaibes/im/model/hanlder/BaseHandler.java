package com.kaibes.im.model.hanlder;

import java.lang.ref.WeakReference;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public abstract class BaseHandler<E> extends Handler {

    // WeakReference这是弱引用，当然也可以不这么写，但是为了优化
    private WeakReference<E> weakReference;

    public BaseHandler(Looper looper, E weakReference) {
	super(looper);
	this.weakReference = new WeakReference<E>(weakReference);
    }

    public BaseHandler(E weakReference) {
	this.weakReference = new WeakReference<E>(weakReference);
    }

    @Override
    public abstract void handleMessage(Message msg);

    /**
     * @return the KaibesActivity
     */
    public E getData() {
	return weakReference.get();
    }

}
