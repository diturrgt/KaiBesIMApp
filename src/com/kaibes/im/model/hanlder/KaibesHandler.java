package com.kaibes.im.model.hanlder;

import com.kaibes.im.control.activity.KaibesActivity;
import com.kaibes.im.model.data.AlertDialogData;
import com.kaibes.im.model.data.ProgressDialogData;
import com.kaibes.im.model.data.ToastData;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Looper;
import android.os.Message;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class KaibesHandler extends BaseHandler<KaibesActivity> {

    public static final int SHOW_TOAST = 0x1;
    public static final int SHOW_ALERT_DIALOG = 0x2;
    public static final int SHOW_PROGRESS_DIALOG = 0x3;
    public static final int CLOSE_PROGRESS_DIALOG = 0x4;

    public KaibesHandler(Looper looper, KaibesActivity weakReference) {
	super(looper, weakReference);
	// TODO Auto-generated constructor stub
    }

    public KaibesHandler(KaibesActivity weakReference) {
	super(weakReference);
	// TODO Auto-generated constructor stub
    }

    @Override
    public void handleMessage(final Message msg) {

	switch (msg.what) {
	case SHOW_TOAST: {
	    Toast toast = Toast.makeText(getData(), getData().getToastData()
		    .getMessage(), getData().getToastData().getTime());
	    toast.setGravity(getData().getToastData().getGravity(), getData()
		    .getToastData().getxOffset(), getData().getToastData()
		    .getyOffset());
	    // toast.setMargin(horizontalMargin, verticalMargin);

	    switch (getData().getToastData().getStyle()) {
	    case ToastData.STYLE_DEFAULT: {
		break;
	    }
	    case ToastData.STYLE_IMAGE: {
		LinearLayout layout = (LinearLayout) toast.getView();
		ImageView imageView = new ImageView(getData());
		imageView.setImageResource(getData().getToastData()
			.getImageId());
		layout.addView(imageView);
		break;
	    }

	    case ToastData.STYLE_VIEW: {
		toast.setView(getData().getToastData().getView());
		break;
	    }

	    default:
		break;
	    }
	    toast.show();
	    break;
	}

	case SHOW_ALERT_DIALOG: {
	    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getData());
	    alertDialog.setTitle(getData().getAlertDialogData().getTitle());

	    alertDialog.setPositiveButton(getData().getAlertDialogData()
		    .getPositiveString(), getData().getAlertDialogData()
		    .getPositiveListener());

	    alertDialog.setNegativeButton(getData().getAlertDialogData()
		    .getNegativeString(), getData().getAlertDialogData()
		    .getNegativeListener());

	    alertDialog.setNeutralButton(getData().getAlertDialogData()
		    .getNeutralString(), getData().getAlertDialogData()
		    .getNeutralListener());

	    switch (getData().getAlertDialogData().getStyle()) {
	    case AlertDialogData.STYLE_DEFAULT: {
		alertDialog.setMessage(getData().getAlertDialogData()
			.getMessage());
		break;
	    }
	    case AlertDialogData.STYLE_ITEMS: {
		alertDialog.setItems(getData().getAlertDialogData()
			.getItemsId(), getData().getAlertDialogData()
			.getItemsListener());
		break;
	    }
	    case AlertDialogData.STYLE_SINGLE_CHOICE: {
		alertDialog.setSingleChoiceItems(getData().getAlertDialogData()
			.getItemsId(), getData().getAlertDialogData()
			.getCheckedItem(), getData().getAlertDialogData()
			.getItemsListener());
		break;
	    }
	    case AlertDialogData.STYLE_MULTI_CHOICE: {
		alertDialog.setMultiChoiceItems(getData().getAlertDialogData()
			.getItemsId(), getData().getAlertDialogData()
			.getCheckedItems(), getData().getAlertDialogData()
			.getMultiChoiceListener());
		break;
	    }
	    case AlertDialogData.STYLE_VIEW: {
		alertDialog.setView(getData().getAlertDialogData().getView());
		break;
	    }

	    default:
		break;
	    }

	    alertDialog.show();
	    break;
	}

	case SHOW_PROGRESS_DIALOG:
	    ProgressDialog progressDialog = new ProgressDialog(getData());
	    ProgressDialogData progressDialogData = getData()
		    .getProgressDialogData();

	    getData().setProgressDialog(progressDialog);

	    progressDialog.setProgressStyle(progressDialogData
		    .getProgressStyle());
	    progressDialog.setCancelable(progressDialogData.isCancelable());
	    progressDialog.setCanceledOnTouchOutside(progressDialogData
		    .isCanceledOnTouchOutside());

	    progressDialog.setIcon(progressDialogData.getIcon());
	    progressDialog.setTitle(progressDialogData.getTitle());
	    progressDialog.setMessage(progressDialogData.getMessage());

	    progressDialog.setOnDismissListener(progressDialogData
		    .getDismissListener());
	    progressDialog.setOnCancelListener(progressDialogData
		    .getCancelListener());
	    progressDialog
		    .setOnKeyListener(progressDialogData.getKeyListener());
	    progressDialog.setOnShowListener(progressDialogData
		    .getShowlistener());

	    progressDialog.show();
	    break;

	case CLOSE_PROGRESS_DIALOG:
	    getData().getProgressDialog().dismiss();
	    break;

	default:
	    break;
	}

    }

}
