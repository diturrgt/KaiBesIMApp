package com.kaibes.im.model.data;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.database.SQLException;

public class ContactsInformationData implements Serializable {

    /**
     * V1.0
     */
    private static final long serialVersionUID = 1L;

    private ArrayList<HashMap<String, String>> informations = null;
    private ArrayList<Boolean> onlines = null;

    public ContactsInformationData() {
    }

    public ContactsInformationData(JSONArray jsonArray) {
	this.informations = new ArrayList<HashMap<String, String>>();
	this.onlines = new ArrayList<Boolean>();
	for (int i = 0; i < jsonArray.length(); i++) {
	    JSONObject jsonObject;
	    try {
		jsonObject = jsonArray.getJSONObject(i);
		HashMap<String, String> information = new HashMap<String, String>();

		information.put("contactname",
			jsonObject.getString("contactname"));
		information.put("nickname", jsonObject.getString("nickname"));
		information.put("photo", jsonObject.getString("photo"));
		information.put("sex", jsonObject.getString("sex"));
		information.put("birth", jsonObject.getString("birth"));
		information.put("signature", jsonObject.getString("signature"));
		information.put("introduction",
			jsonObject.getString("introduction"));
		information.put("job", jsonObject.getString("job"));
		information.put("company", jsonObject.getString("company"));
		information.put("school", jsonObject.getString("school"));
		information.put("address", jsonObject.getString("address"));
		information.put("hometown", jsonObject.getString("hometown"));
		information.put("email", jsonObject.getString("email"));
		information.put("remark", jsonObject.getString("remark"));

		informations.add(information);
		onlines.add(jsonObject.getBoolean("online"));
	    } catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }

    public void addContactInformationsData(ResultSet resultSet, boolean online) {
	try {

	    try {
		while (resultSet.next()) {
		    HashMap<String, String> information = new HashMap<String, String>();
		    information.put("contactname",
			    resultSet.getString("contactname"));
		    information
			    .put("nickname", resultSet.getString("nickname"));
		    information.put("photo", resultSet.getString("photo"));
		    information.put("sex", resultSet.getString("sex"));
		    information.put("birth", resultSet.getString("birth"));
		    information.put("signature",
			    resultSet.getString("signature"));
		    information.put("introduction",
			    resultSet.getString("introduction"));
		    information.put("job", resultSet.getString("job"));
		    information.put("company", resultSet.getString("company"));
		    information.put("school", resultSet.getString("school"));
		    information.put("address", resultSet.getString("address"));
		    information
			    .put("hometown", resultSet.getString("hometown"));
		    information.put("email", resultSet.getString("email"));
		    information.put("remark", resultSet.getString("remark"));
		    informations.add(information);
		}
		resultSet.close();
	    } catch (java.sql.SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	}
	;
    }

    public JSONArray toJSONArray() {
	JSONArray jsonArray = new JSONArray();
	for (int i = 0; i < informations.size(); i++) {
	    HashMap<String, String> information = informations.get(i);
	    JSONObject jsonObject = new JSONObject();
	    try {
		jsonObject.put("contactname", information.get("contactname"));
		jsonObject.put("nickname", information.get("nickname"));
		jsonObject.put("photo", information.get("photo"));
		jsonObject.put("sex", information.get("sex"));
		jsonObject.put("birth", information.get("birth"));
		jsonObject.put("signature", information.get("signature"));
		jsonObject.put("introduction", information.get("introduction"));
		jsonObject.put("job", information.get("job"));
		jsonObject.put("company", information.get("company"));
		jsonObject.put("school", information.get("school"));
		jsonObject.put("address", information.get("address"));
		jsonObject.put("hometown", information.get("hometown"));
		jsonObject.put("email", information.get("email"));
		jsonObject.put("remark", information.get("remark"));

		jsonArray.put(jsonObject);
	    } catch (JSONException e) {
		e.printStackTrace();
	    }

	}

	return jsonArray;
    }

    public ContactInformationData getContactInformationData(int i) {
	return new ContactInformationData(informations.get(i), onlines.get(i));
    }
    
    public HashMap<String, String> getData(int i){
	return informations.get(i);
    }

    public String getContactname(int i) {
	return informations.get(i).get("contactname");
    }

    public String getNickname(int i) {
	return informations.get(i).get("nickname");
    }

    public String getPhoto(int i) {
	return informations.get(i).get("photo");
    }

    public String getSex(int i) {
	return informations.get(i).get("sex");
    }

    public String getBirth(int i) {
	return informations.get(i).get("birth");
    }

    public String getSignature(int i) {
	return informations.get(i).get("signature");
    }

    public String getIntroduction(int i) {
	return informations.get(i).get("introduction");
    }

    public String getJob(int i) {
	return informations.get(i).get("job");
    }

    public String getCompany(int i) {
	return informations.get(i).get("company");
    }

    public String getSchool(int i) {
	return informations.get(i).get("school");
    }

    public String getAddress(int i) {
	return informations.get(i).get("address");
    }

    public String getHometown(int i) {
	return informations.get(i).get("hometown");
    }

    public String getEmail(int i) {
	return informations.get(i).get("email");
    }

    public String getRemark(int i) {
	return informations.get(i).get("remark");
    }

    public boolean isOnline(int i) {
	return onlines.get(i);
    }

    public int length() {
	return informations.size();
    }

}
