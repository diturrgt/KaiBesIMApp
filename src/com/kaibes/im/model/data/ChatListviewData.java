package com.kaibes.im.model.data;

import java.io.Serializable;

public class ChatListviewData implements Serializable{

    /**
     * V1.0
     */
    private static final long serialVersionUID = 1L;
    
    private ContactInformationData contactInformationData = null;
    private String message = "";

    public ChatListviewData() {
    }

    public ChatListviewData(ContactInformationData contactInformationData,
	    String message) {
	this.contactInformationData = contactInformationData;
	this.message = message;
    }

    public ChatListviewData(String message) {
	this.message = message;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    public ContactInformationData getContactInformationData() {
	return contactInformationData;
    }

    public void setContactInformationData(
	    ContactInformationData contactInformationData) {
	this.contactInformationData = contactInformationData;
    }

}
