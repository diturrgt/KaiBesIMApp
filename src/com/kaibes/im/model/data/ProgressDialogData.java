package com.kaibes.im.model.data;

import java.io.Serializable;

import android.app.ProgressDialog;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.DialogInterface.OnShowListener;

import com.kaibes.im.R;

public class ProgressDialogData implements Serializable {

    /**
     * V1.0
     */
    private static final long serialVersionUID = 1L;

    private int progressStyle, icon;
    private boolean cancelable, canceledOnTouchOutside;
    private String title, message;

    private OnDismissListener dismissListener;
    private OnKeyListener keyListener;
    private OnCancelListener cancelListener;
    private OnShowListener showlistener;

    private OnClickListener positiveClickListener, negativeClickListener,
	    neutralClickListener;

    /**
     * 进度条数据，默认是水平方向的，可以按后退键取消的，不能点其他位置取消的，无标题的，无图标的，无监听器的
     */
    public ProgressDialogData() {
	reset();
    }

    public ProgressDialogData reset() {
	setProgressStyle(ProgressDialog.STYLE_SPINNER);
	setCancelable(false);
	setCanceledOnTouchOutside(false);
	setIcon(R.drawable.ic_launcher);

	setDismissListener(null);
	setKeyListener(null);
	setCancelListener(null);
	setShowlistener(null);

	setTitle("");
	setMessage("");

	return this;
    }

    // ---------------------------GET AND SET 1--------------------------

    /**
     * 获取进度条风格
     * 
     * @author QiuShuiCai
     * @return int 进度条风格的值
     */
    public int getProgressStyle() {
	return progressStyle;
    }

    /**
     * 设置进度条风格，有水平和圆形两种，默认是水平方向的，如ProgressDialog.STYLE_HORIZONTAL
     * 
     * @param progressStyle
     *            ProgressDialog.STYLE_HORIZONTAL或者ProgressDialog.STYLE_SPINNER
     * @author QiuShuiCai
     */
    public ProgressDialogData setProgressStyle(int progressStyle) {
	this.progressStyle = progressStyle;
	return this;
    }

    /**
     * 获取是否可以通过点击Back键取消进度条
     * 
     * @author QiuShuiCai
     * @return boolean 是否可以通过点击Back键取消进度条
     */
    public boolean isCancelable() {
	return cancelable;
    }

    /**
     * 设置是否可以通过点击Back键取消进度条
     * 
     * @param cancelable
     *            是否可以通过点击Back键取消进度条
     * @author QiuShuiCai
     */
    public ProgressDialogData setCancelable(boolean cancelable) {
	this.cancelable = cancelable;
	return this;
    }

    /**
     * 获取在点击Dialog外是否取消Dialog进度条
     * 
     * @author QiuShuiCai
     * @return boolean
     */
    public boolean isCanceledOnTouchOutside() {
	return canceledOnTouchOutside;
    }

    /**
     * 设置在点击Dialog外是否取消Dialog进度条
     * 
     * @param canceledOnTouchOutside
     *            在点击Dialog外是否取消Dialog进度条
     * @author QiuShuiCai
     */
    public ProgressDialogData setCanceledOnTouchOutside(
	    boolean canceledOnTouchOutside) {
	this.canceledOnTouchOutside = canceledOnTouchOutside;
	return this;
    }

    /**
     * 获取提示的title的图标
     * 
     * @author QiuShuiCai
     * @return int
     */
    public int getIcon() {
	return icon;
    }

    /**
     * 设置提示的title的图标
     * 
     * @param icon
     *            图标的ID
     * @author QiuShuiCai
     */
    public ProgressDialogData setIcon(int icon) {
	this.icon = icon;
	return this;
    }

    /**
     * 获取提示的标题
     * 
     * @author QiuShuiCai
     * @return String
     */
    public String getTitle() {
	return title;
    }

    /**
     * 设置提示的标题
     * 
     * @param title
     *            标题
     * @author QiuShuiCai
     */
    public ProgressDialogData setTitle(String title) {
	this.title = title;
	return this;
    }

    /**
     * 获取dismiss监听器
     * 
     * @author QiuShuiCai
     * @return OnDismissListener
     */
    public OnDismissListener getDismissListener() {
	return dismissListener;
    }

    /**
     * 设置dismiss监听器
     * 
     * @param onDismissListener
     *            dismiss监听器
     * @author QiuShuiCai
     * @return void
     */
    public ProgressDialogData setDismissListener(
	    OnDismissListener dismissListener) {
	this.dismissListener = dismissListener;
	return this;
    }

    /**
     * 获取onkey监听器
     * 
     * @author QiuShuiCai
     * @return OnKeyListener
     */
    public OnKeyListener getKeyListener() {
	return keyListener;
    }

    /**
     * 设置onkey监听器
     * 
     * @param onKeyListener
     *            onkey监听器
     * @author QiuShuiCai
     */
    public ProgressDialogData setKeyListener(OnKeyListener keyListener) {
	this.keyListener = keyListener;
	return this;
    }

    /**
     * 获取cancel监听器
     * 
     * @author QiuShuiCai
     * @return OnCancelListener
     */
    public OnCancelListener getCancelListener() {
	return cancelListener;
    }

    /**
     * 设置cancel监听器
     * 
     * @param keyListener
     *            cancel监听器
     * @author QiuShuiCai
     */
    public ProgressDialogData setCancelListener(OnCancelListener cancelListener) {
	this.cancelListener = cancelListener;
	return this;
    }

    /**
     * 获取确定按钮监听器
     * 
     * @author QiuShuiCai
     * @return OnClickListener
     */
    public OnClickListener getPositiveClickListener() {
	return positiveClickListener;
    }

    /**
     * 设置确定按钮监听器
     * 
     * @param keyListener
     *            确定按钮监听器
     * @author QiuShuiCai
     */
    public ProgressDialogData setPositiveClickListener(
	    OnClickListener positiveClickListener) {
	this.positiveClickListener = positiveClickListener;
	return this;
    }

    /**
     * 获取否定按钮监听器
     * 
     * @author QiuShuiCai
     * @return OnClickListener
     */
    public OnClickListener getNegativeClickListener() {
	return negativeClickListener;
    }

    /**
     * 设置否定按钮监听器
     * 
     * @param keyListener
     *            否定按钮监听器
     * @author QiuShuiCai
     */
    public ProgressDialogData setNegativeClickListener(
	    OnClickListener negativeClickListener) {
	this.negativeClickListener = negativeClickListener;
	return this;
    }

    /**
     * 获取中立按钮监听器
     * 
     * @author QiuShuiCai
     * @return OnClickListener
     */
    public OnClickListener getNeutralClickListener() {
	return neutralClickListener;
    }

    /**
     * 设置中立按钮监听器
     * 
     * @param keyListener
     *            中立按钮监听器
     * @author QiuShuiCai
     */
    public ProgressDialogData setNeutralClickListener(
	    OnClickListener neutralClickListener) {
	this.neutralClickListener = neutralClickListener;
	return this;
    }

    public String getMessage() {
	return message;
    }

    public ProgressDialogData setMessage(String message) {
	this.message = message;
	return this;
    }

    public OnShowListener getShowlistener() {
	return showlistener;
    }

    public void setShowlistener(OnShowListener showlistener) {
	this.showlistener = showlistener;
    }

}
