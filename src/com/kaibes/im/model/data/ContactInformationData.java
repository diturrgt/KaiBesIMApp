package com.kaibes.im.model.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import android.database.Cursor;

import com.kaibes.im.utils.BirthUtil;

public class ContactInformationData implements Serializable {

    /**
     * V1.0
     */
    private static final long serialVersionUID = 1L;

    private String message = null;
    private boolean online;
//    exitstConversation = false;

    private HashMap<String, String> data = null;
    private ArrayList<ChatListviewData> chatListviewDatas;

    public ContactInformationData(HashMap<String, String> data, boolean b) {
	setData(data);
	setOnline(b);
	setChatListviewDatas(new ArrayList<ChatListviewData>());
    }

    public ContactInformationData(Cursor cursor, boolean b) {
	setData(new HashMap<String, String>());

	cursor.moveToPosition(0);
	setCursor(cursor, b);

	setOnline(b);
	setChatListviewDatas(new ArrayList<ChatListviewData>());
    }

    // ---------------------------GET AND SET 1--------------------------

    public void setCursor(Cursor cursor, boolean b) {
	// int idColum = cursor.getColumnIndex("_id");
	int usernameColumn = cursor.getColumnIndex("contactname");
	int nicknameColumn = cursor.getColumnIndex("nickname");
	int photoColumn = cursor.getColumnIndex("photo");
	int sexColumn = cursor.getColumnIndex("sex");
	int birthColumn = cursor.getColumnIndex("birth");
	int signatureColumn = cursor.getColumnIndex("signature");
	int introductionColumn = cursor.getColumnIndex("introduction");
	int jobColumn = cursor.getColumnIndex("job");
	int companyColumn = cursor.getColumnIndex("company");
	int schoolColumn = cursor.getColumnIndex("school");
	int addressColumn = cursor.getColumnIndex("address");
	int hometownColumn = cursor.getColumnIndex("hometown");
	int emailColumn = cursor.getColumnIndex("email");
	int remarkColumn = cursor.getColumnIndex("remark");

	getData().put("contactname", cursor.getString(usernameColumn));
	getData().put("nickname", cursor.getString(nicknameColumn));
	getData().put("photo", cursor.getString(photoColumn));
	getData().put("sex", cursor.getString(sexColumn));
	getData().put("birth", cursor.getString(birthColumn));
	getData().put("signature", cursor.getString(signatureColumn));
	getData().put("introduction", cursor.getString(introductionColumn));
	getData().put("job", cursor.getString(jobColumn));
	getData().put("company", cursor.getString(companyColumn));
	getData().put("school", cursor.getString(schoolColumn));
	getData().put("address", cursor.getString(addressColumn));
	getData().put("hometown", cursor.getString(hometownColumn));
	getData().put("email", cursor.getString(emailColumn));
	getData().put("remark", cursor.getString(remarkColumn));

	// data.put("online", b);
    }

    public String getContent(String key) {
	String content = "";

	switch (key) {
	case "age":
	    content = String.valueOf(getAge());
	    break;

	case "zodica":
	    content = getZodica();
	    break;

	case "constellation":
	    content = getConstellation();
	    break;

	// case "online":
	// content = (boolean) data.get(key) ? "true":"false";
	// break;

	default:
	    content = data.get(key);
	    break;
	}

	return content;
    }

    public void setValue(String key, String value) {
	data.put(key, value);
    }

    public String getValue(String key) {
	return data.get(key);
    }
    
    public String getContactname() {
	return (String) data.get("contactname");
    }

    public void setContactname(String contactname) {
	data.put("contactname", contactname);
    }

    public String getNickname() {
	return data.get("nickname");
    }

    public void setNickname(String nickname) {
	data.put("nickname", nickname);
    }

    public String getPhoto() {
	return data.get("photo");
    }

    public void setPhoto(String photo) {
	data.put("photo", photo);
    }

    public String getBirth() {
	return data.get("birth");
    }

    public void setBirth(String birth) {
	data.put("birth", birth);
    }

    public int getAge() {
	return BirthUtil.getAge(getBirth());
    }

    public String getZodica() {
	return BirthUtil.getZodica(getBirth());
    }

    public String getConstellation() {
	return BirthUtil.getConstellation(getBirth());
    }

    public String getInformation() {
	return getSex() + " " + getAge() + " " + getAddress();
    }

    public String getEmail() {
	return data.get("email");
    }

    public void setEmail(String email) {
	data.put("email", email);
    }

    public String getSignature() {
	return data.get("signature");
    }

    public void setSignature(String signature) {
	data.put("signature", signature);
    }

    public String getRemark() {
	return data.get("remark");
    }

    public void setRemark(String remark) {
	data.put("remark", remark);
    }

    public String getSex() {
	return data.get("sex");
    }

    public void setSex(String sex) {
	data.put("sex", sex);
    }

    public String getIntroduction() {
	return data.get("introduction");
    }

    public void setIntroduction(String introduction) {
	data.put("introduction", introduction);
    }

    public String getJob() {
	return data.get("job");
    }

    public void setJob(String job) {
	data.put("job", job);
    }

    public String getCompany() {
	return data.get("company");
    }

    public void setCompany(String company) {
	data.put("company", company);
    }

    public String getSchool() {
	return data.get("school");
    }

    public void setSchool(String school) {
	data.put("school", school);
    }

    public String getAddress() {
	return data.get("address");
    }

    public void setAddress(String address) {
	data.put("address", address);
    }

    public String getHometown() {
	return data.get("hometown");
    }

    public void setHometown(String hometown) {
	data.put("hometown", hometown);
    }

    // ---------------------------GET AND SET 2--------------------------
    private HashMap<String, String> getData() {
	return data;
    }

    public void setData(HashMap<String, String> data) {
	this.data = data;
    }

    public ArrayList<ChatListviewData> getChatListviewDatas() {
	return chatListviewDatas;
    }

    private void setChatListviewDatas(
	    ArrayList<ChatListviewData> chatListviewDatas) {
	this.chatListviewDatas = chatListviewDatas;
    }

    public void addChatListviewData(String message) {
	this.chatListviewDatas.add(new ChatListviewData(this, message));
	setMessage(message);
    }

    public void addChatListviewData(String message,
	    UserInformationData userInformationData) {
	this.chatListviewDatas.add(new ChatListviewData(message));
	setMessage(message);
    }

    public boolean isOnline() {
	return online;
    }

    public void setOnline(boolean online) {
	this.online = online;
    }

    public String getMessage() {
	return message;
    }

    private void setMessage(String message) {
	this.message = message;
    }

//    public boolean isExitstConversation() {
//	return exitstConversation;
//    }
//
//    public void setExitstConversation(boolean exitstConversation) {
//	this.exitstConversation = exitstConversation;
//    }

}
