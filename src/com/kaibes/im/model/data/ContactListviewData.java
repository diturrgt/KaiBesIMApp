package com.kaibes.im.model.data;

import java.io.Serializable;
import java.util.ArrayList;

import com.kaibes.im.KaibesApplication;

public class ContactListviewData implements Serializable {

    /**
     * V1.0
     */

    private static final long serialVersionUID = 1L;
    private KaibesApplication kaibesimApplication;
    private String name, key;

    public ContactListviewData(KaibesApplication kaibesimApplication,
	    String name, String key) {
	setKaibesApplication(kaibesimApplication);
	setName(name);
	setKey(key);
    }

    public String getContent() {
	return getKaibesApplication().getCurentContact().getContent(
		getKey());
    }

    public static ArrayList<ContactListviewData> getArraylist(
	    KaibesApplication kaibesimApplication) {
	ArrayList<ContactListviewData> datas = new ArrayList<ContactListviewData>();
	datas.add(new ContactListviewData(kaibesimApplication, "备注", "remark"));
	datas.add(new ContactListviewData(kaibesimApplication, "昵称", "nickname"));
	datas.add(new ContactListviewData(kaibesimApplication, "性别", "sex"));
	datas.add(new ContactListviewData(kaibesimApplication, "年龄", "age"));
	datas.add(new ContactListviewData(kaibesimApplication, "星座",
		"constellation"));
	datas.add(new ContactListviewData(kaibesimApplication, "职业", "job"));
	datas.add(new ContactListviewData(kaibesimApplication, "公司", "company"));
	datas.add(new ContactListviewData(kaibesimApplication, "学校", "school"));
	datas.add(new ContactListviewData(kaibesimApplication, "所在地", "address"));
	datas.add(new ContactListviewData(kaibesimApplication, "故乡", "hometown"));
	datas.add(new ContactListviewData(kaibesimApplication, "邮箱", "email"));
	datas.add(new ContactListviewData(kaibesimApplication, "个人说明",
		"introduction"));
	return datas;
    }

    // ---------------------------GET AND SET 1--------------------------
    public String getName() {
	return name;
    }

    private void setName(String name) {
	this.name = name;
    }

    private KaibesApplication getKaibesApplication() {
	return kaibesimApplication;
    }

    private void setKaibesApplication(KaibesApplication kaibesimApplication) {
	this.kaibesimApplication = kaibesimApplication;
    }

    private String getKey() {
	return key;
    }

    private void setKey(String key) {
	this.key = key;
    }

}
