package com.kaibes.im.model.data;

import java.io.Serializable;

public class PluginListviewData implements Serializable {

    /**
     * V1.0
     */
    private static final long serialVersionUID = 1L;
    
    private int id, action;
    private String contactname, message;

    public PluginListviewData() {
	this(0, 0, "", "");
    }

    public PluginListviewData(int id, int action, String contactname,
	    String message) {
	setId(id);
	setAction(action);
	setContactname(contactname);
	setMessage(message);
    }

    public String getContactname() {
	return contactname;
    }

    public void setContactname(String contactname) {
	this.contactname = contactname;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public int getAction() {
	return action;
    }

    public void setAction(int action) {
	this.action = action;
    }

}
