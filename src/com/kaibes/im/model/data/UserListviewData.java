package com.kaibes.im.model.data;

import java.io.Serializable;
import java.util.ArrayList;

public class UserListviewData implements Serializable {

    /**
     * V1.0
     */
    private static final long serialVersionUID = 1L;

    private UserInformationData userInformationData;
    private String name, key;

    public UserListviewData(UserInformationData userInformationData,
	    String name, String key) {
	this.userInformationData = userInformationData;
	this.name = name;
	this.key = key;
    }

    public static ArrayList<UserListviewData> getArraylist(
	    UserInformationData userInformationData) {
	ArrayList<UserListviewData> datas = new ArrayList<UserListviewData>();
	datas.add(new UserListviewData(userInformationData, "昵称", "nickname"));
	datas.add(new UserListviewData(userInformationData, "性别", "sex"));
	datas.add(new UserListviewData(userInformationData, "生日", "birth"));
	datas.add(new UserListviewData(userInformationData, "年龄", "age"));
	datas.add(new UserListviewData(userInformationData, "星座",
		"constellation"));
	datas.add(new UserListviewData(userInformationData, "职业", "job"));
	datas.add(new UserListviewData(userInformationData, "公司", "company"));
	datas.add(new UserListviewData(userInformationData, "学校", "school"));
	datas.add(new UserListviewData(userInformationData, "所在地", "address"));
	datas.add(new UserListviewData(userInformationData, "故乡", "hometown"));
	datas.add(new UserListviewData(userInformationData, "邮箱", "email"));
	datas.add(new UserListviewData(userInformationData, "个人说明",
		"introduction"));
	datas.add(new UserListviewData(userInformationData, "个性签名",
		"signature"));
	return datas;
    }

    public String getName() {
	return name;
    }

    public String getKey() {
	return key;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getContent() {
	return userInformationData.getContent(key);
    }

    public void setContent(String content) {
	userInformationData.setContent(key, content);
    }

}
