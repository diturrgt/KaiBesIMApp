package com.kaibes.im.model.data;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.kaibes.im.utils.BirthUtil;

public class UserInformationData implements Serializable {

    /**
     * V1.0
     */
    private static final long serialVersionUID = 1L;

    private JSONObject jsonObject;

    public UserInformationData(JSONObject jsonObject) {
	this.jsonObject = jsonObject;
    }

    public JSONObject getJsonObject() {
	return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
	this.jsonObject = jsonObject;
    }

    public String getContent(String key) {
	try {
	    String content = "";

	    switch (key) {
	    case "age":
		content = String.valueOf(BirthUtil.getAge(getBirth()));
		break;

	    case "zodica":
		content = BirthUtil.getZodica(getBirth());
		break;

	    case "constellation":
		content = BirthUtil.getConstellation(getBirth());
		break;

	    default:
		content = jsonObject.getString(key);
		break;
	    }
	    return content;
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setContent(String key, String content) {
	try {
	    jsonObject.put(key, content);
	} catch (JSONException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public String getUsername() {
	try {
	    return jsonObject.getString("username");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setUsername(String username) {
	try {
	    jsonObject.put("username", username);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getNickname() {
	try {
	    return jsonObject.getString("nickname");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setNickname(String nickname) {
	try {
	    jsonObject.put("nickname", nickname);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getPhoto() {
	try {
	    return jsonObject.getString("photo");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setPhoto(String photo) {
	try {
	    jsonObject.put("photo", photo);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getSex() {
	try {
	    return jsonObject.getString("sex");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setSex(String sex) {
	try {
	    jsonObject.put("sex", sex);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getJob() {
	try {
	    return jsonObject.getString("job");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setJob(String job) {
	try {
	    jsonObject.put("job", job);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getCompany() {
	try {
	    return jsonObject.getString("company");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setCompany(String company) {
	try {
	    jsonObject.put("company", company);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getSchool() {
	try {
	    return jsonObject.getString("school");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setSchool(String school) {
	try {
	    jsonObject.put("school", school);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getAddress() {
	try {
	    return jsonObject.getString("address");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setAddress(String address) {
	try {
	    jsonObject.put("address", address);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getHometown() {
	try {
	    return jsonObject.getString("hometown");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setHometown(String hometown) {
	try {
	    jsonObject.put("hometown", hometown);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getEmail() {
	try {
	    return jsonObject.getString("email");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setEmail(String email) {
	try {
	    jsonObject.put("email", email);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getSignature() {
	try {
	    return jsonObject.getString("signature");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setSignature(String signature) {
	try {
	    jsonObject.put("signature", signature);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getIntroduction() {
	try {
	    return jsonObject.getString("introduction");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setIntroduction(String introduction) {
	try {
	    jsonObject.put("introduction", introduction);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public String getBirth() {
	try {
	    return jsonObject.getString("birth");
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void setBirth(String birth) {
	try {
	    jsonObject.put("birth", birth);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

}
