package com.kaibes.im.model.data;

import java.io.Serializable;

import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

public class ToastData implements Serializable {

    /**
     * V1.0
     */
    private static final long serialVersionUID = 1L;

    /**
     * 默认模式
     */
    public static final int STYLE_DEFAULT = 1;
    /**
     * 加图模式
     */
    public static final int STYLE_IMAGE = 2;
    /**
     * 自定义布局模式
     */
    public static final int STYLE_VIEW = 3;

    private int style, time, gravity, xOffset, yOffset, imageId;
    private String message;
    private View view;

    /**
     * 默认模式，短时间显示，无内容
     */
    public ToastData() {
	initialize();
    }

    private void initialize() {
	reset();
    }

    public ToastData reset() {
	setStyle(STYLE_DEFAULT);
	setTime(Toast.LENGTH_SHORT);
	setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
	setxOffset(0);
	setyOffset(0);
	setMessage("");
	setImageId(0);
	setView(null);

	return this;
    }

    // ---------------------------GET AND SET 1--------------------------
    /**
     * 获取显示的风格
     * 
     * @author QiuShuiCai
     * @return int
     */
    public int getStyle() {
	return style;
    }

    /**
     * 设置显示的风格，如TostData.STYLE_DEFAULT
     * 
     * @param style
     *            风格类型
     * @author QiuShuiCai
     */
    public void setStyle(int style) {
	this.style = style;
    }

    /**
     * 获取显示的时间
     * 
     * @author QiuShuiCai
     * @return int
     */
    public int getTime() {
	return time;
    }

    /**
     * 设置显示的时间，如Toast.LENGTH_SHORT
     * 
     * @param time
     * @author QiuShuiCai
     */
    public ToastData setTime(int time) {
	this.time = time;
	return this;
    }

    /**
     * 获取显示的内容
     * 
     * @author QiuShuiCai
     * @return String
     */
    public String getMessage() {
	return message;
    }

    /**
     * 设置显示的内容
     * 
     * @param content
     * @author QiuShuiCai
     */
    public void setMessage(String message) {
	this.message = message;
    }

    public int getGravity() {
	return gravity;
    }

    /**
     * 设置对齐方向，如Gravity.CENTER
     * 
     * @param gravity
     *            对齐方向
     * @author QiuShuiCai
     */
    public void setGravity(int gravity) {
	this.gravity = gravity;
    }

    public int getxOffset() {
	return xOffset;
    }

    public void setxOffset(int xOffset) {
	this.xOffset = xOffset;
    }

    public int getyOffset() {
	return yOffset;
    }

    public void setyOffset(int yOffset) {
	this.yOffset = yOffset;
    }

    public int getImageId() {
	return imageId;
    }

    public void setImageId(int imageId) {
	this.imageId = imageId;
    }

    public View getView() {
	return view;
    }

    /**
     * 设置自定义布局
     * 
     * @param view
     *            布局
     * @author QiuShuiCai
     */
    public void setView(View view) {
	this.view = view;
    }

}
