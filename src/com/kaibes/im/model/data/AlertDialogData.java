package com.kaibes.im.model.data;

import java.io.Serializable;

import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.view.View;

public class AlertDialogData implements Serializable {

    /**
     * V1.0
     */
    private static final long serialVersionUID = 1L;

    /**
     * 默认模式，消息模式
     */
    public static final int STYLE_DEFAULT = 1;
    /**
     * 单选模式
     */
    public static final int STYLE_SINGLE_CHOICE = 2;
    /**
     * 多选模式
     */
    public static final int STYLE_MULTI_CHOICE = 3;
    /**
     * 列表框模式
     */
    public static final int STYLE_ITEMS = 4;
    /**
     * 自定义布局模式
     */
    public static final int STYLE_VIEW = 5;

    private int style, itemsId, checkedItem;
    private boolean[] checkedItems;
    private String title, message, positiveString, negativeString,
	    neutralString;
    private OnClickListener positiveListener, negativeListener,
	    neutralListener, itemsListener;
    private OnMultiChoiceClickListener multiChoiceListener;

    private View view;

    public AlertDialogData() {
	initialize();
    }

    private void initialize() {
	reset();
    }

    public AlertDialogData reset() {
	setStyle(STYLE_DEFAULT);

	setTitle("标题");
	setMessage("消息");

	setPositiveString("确定");
	setNegativeString("");
	setNeutralString("");
	setPositiveListener(null);
	setNegativeListener(null);
	setNeutralListener(null);

	setItemsId(0);
	setCheckedItem(0);
	setCheckedItems(null);

	setItemsListener(null);
	setMultiChoiceListener(null);
	setView(null);

	return this;
    }

    // ---------------------------GET AND SET 1--------------------------
    public String getTitle() {
	return title;
    }

    public AlertDialogData setTitle(String title) {
	this.title = title;
	return this;
    }

    public String getMessage() {
	return message;
    }

    public AlertDialogData setMessage(String message) {
	this.message = message;
	return this;
    }

    public String getPositiveString() {
	return positiveString;
    }

    public AlertDialogData setPositiveString(String positiveString) {
	this.positiveString = positiveString;
	return this;
    }

    public String getNegativeString() {
	return negativeString;
    }

    public AlertDialogData setNegativeString(String negativeString) {
	this.negativeString = negativeString;
	return this;
    }

    public String getNeutralString() {
	return neutralString;
    }

    public AlertDialogData setNeutralString(String neutralString) {
	this.neutralString = neutralString;
	return this;
    }

    public int getStyle() {
	return style;
    }

    public AlertDialogData setStyle(int style) {
	this.style = style;
	return this;
    }

    public OnClickListener getPositiveListener() {
	return positiveListener;
    }

    public AlertDialogData setPositiveListener(OnClickListener positiveListener) {
	this.positiveListener = positiveListener;
	return this;
    }

    public OnClickListener getNegativeListener() {
	return negativeListener;
    }

    public AlertDialogData setNegativeListener(OnClickListener negativeListener) {
	this.negativeListener = negativeListener;
	return this;
    }

    public OnClickListener getNeutralListener() {
	return neutralListener;
    }

    public AlertDialogData setNeutralListener(OnClickListener neutralListener) {
	this.neutralListener = neutralListener;
	return this;
    }

    public OnClickListener getItemsListener() {
	return itemsListener;
    }

    public AlertDialogData setItemsListener(OnClickListener itemsListener) {
	this.itemsListener = itemsListener;
	return this;
    }

    public int getItemsId() {
	return itemsId;
    }

    public AlertDialogData setItemsId(int itemsId) {
	this.itemsId = itemsId;
	return this;
    }

    public int getCheckedItem() {
	return checkedItem;
    }

    public AlertDialogData setCheckedItem(int checkedItem) {
	this.checkedItem = checkedItem;
	return this;
    }

    public boolean[] getCheckedItems() {
	return checkedItems;
    }

    public AlertDialogData setCheckedItems(boolean[] checkedItems) {
	this.checkedItems = checkedItems;
	return this;
    }

    public OnMultiChoiceClickListener getMultiChoiceListener() {
	return multiChoiceListener;
    }

    public AlertDialogData setMultiChoiceListener(
	    OnMultiChoiceClickListener multiChoiceListener) {
	this.multiChoiceListener = multiChoiceListener;
	return this;
    }

    public View getView() {
	return view;
    }

    public AlertDialogData setView(View view) {
	this.view = view;
	return this;
    }

}
