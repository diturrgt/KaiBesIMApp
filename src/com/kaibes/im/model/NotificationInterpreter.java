package com.kaibes.im.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.kaibes.im.KaibesApplication;
import com.kaibes.im.R;
import com.kaibes.im.model.data.PluginListviewData;
import com.kaibes.im.utils.JsonParserUtil;
import com.kaibes.im.view.fragment.KaibesFragment;
import com.kaibes.im.view.fragment.MainFragment;

public class NotificationInterpreter {

    public final static int notification_id = 2;

    public final static int ACTION = 16;
    public final static int ACTION_MESSAGE = 1;

    @TargetApi(Build.VERSION_CODES.L)
    public static void explainClient(JsonParserUtil jsonParserUtil,
	    KaibesApplication kaibesApplication) {

	JSONArray jsonArray = jsonParserUtil.getJsonArraySon();
	ArrayList<PluginListviewData> datas = new ArrayList<PluginListviewData>();

	for (int i = 0; i < jsonArray.length(); i++) {
	    try {
		datas.add(explain(jsonArray.getJSONObject(i), kaibesApplication));
	    } catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
	kaibesApplication.getPluginListviewDatas().addAll(datas);

	if (kaibesApplication.isMainActivity()) {
	    kaibesApplication.refresh(KaibesFragment.MAIN_FRAGMENT,
		    MainFragment.REFRESH_PLUGIN);
	    PluginListviewData pluginListviewData = kaibesApplication
		    .getPluginListviewDatas()
		    .get(kaibesApplication.getPluginListviewDatas().size() - 1);
	    kaibesApplication
		    .getCurrentActivity()
		    .getIntent()
		    .putExtra("notification",
			    MainFragment.NOTIFICATION_NOTIFICATION);

	    PendingIntent pendingIntent = PendingIntent.getActivity(
		    kaibesApplication.getCurrentActivity(), 0,
		    kaibesApplication.getCurrentActivity().getIntent(),
		    PendingIntent.FLAG_UPDATE_CURRENT);

	    Notification notification = null;
	    if (Integer.valueOf(android.os.Build.VERSION.SDK) < 21) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(
			kaibesApplication.getCurrentActivity());

		notification = builder
			.setTicker(pluginListviewData.getMessage())
			.setSmallIcon(R.drawable.ic_launcher)
			.setLargeIcon(
				BitmapFactory.decodeResource(
					kaibesApplication.getResources(),
					R.drawable.ic_launcher))
			.setContentTitle(pluginListviewData.getContactname())
			.setContentText(pluginListviewData.getMessage())
			.setAutoCancel(true).setContentIntent(pendingIntent)
			.setDefaults(Notification.DEFAULT_ALL).build();

	    } else {
		Notification.Builder builder = new Notification.Builder(
			kaibesApplication.getCurrentActivity());
		notification = builder
			.setSmallIcon(R.drawable.ic_launcher)
			.setLargeIcon(
				BitmapFactory.decodeResource(
					kaibesApplication.getResources(),
					R.drawable.ic_launcher))
			.setContentTitle(pluginListviewData.getContactname())
			.setContentText(pluginListviewData.getMessage())
			.setVisibility(Notification.VISIBILITY_PUBLIC)
			.setPriority(Notification.PARCELABLE_WRITE_RETURN_VALUE)
			.setAutoCancel(true).setContentIntent(pendingIntent)
			.setSound(Uri.parse("android.resource://com.kaibes.im/raw/gm.mp3"))
			.setDefaults(Notification.DEFAULT_ALL).build();
	    }

	    NotificationManagerCompat manager = NotificationManagerCompat
		    .from(kaibesApplication.getCurrentActivity());
	    manager.cancel(notification_id);
	    manager.notify(notification_id, notification);
	}
    }

    private static PluginListviewData explain(JSONObject jsonObject,
	    KaibesApplication kaibesimApplication) {
	try {
	    if (jsonObject.getInt("id") == -1) {
		kaibesimApplication.removeTempSearchData(jsonObject
			.getString("contactname"));
	    } else if (jsonObject.getInt("id") == 0) {
		kaibesimApplication.putTempSearchData(jsonObject
			.getString("contactname"));
	    }
	    return new PluginListviewData(jsonObject.getInt("id"),
		    jsonObject.getInt("action"),
		    jsonObject.getString("contactname"),
		    jsonObject.getString("message"));
	} catch (JSONException e) {
	    e.printStackTrace();
	    return null;
	}

    }

}
