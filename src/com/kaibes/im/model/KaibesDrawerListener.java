package com.kaibes.im.model;

import com.kaibes.im.KaibesApplication;
import com.kaibes.im.R;
import com.kaibes.im.view.RoundImageView;
import com.nineoldandroids.view.ViewHelper;

import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

public class KaibesDrawerListener implements DrawerListener {

    private DrawerLayout drawerLayout;
    private KaibesApplication kaibesApplication;

    public KaibesDrawerListener(DrawerLayout drawerLayout,
	    KaibesApplication kaibesApplication) {
	this.kaibesApplication = kaibesApplication;
	this.drawerLayout = drawerLayout;
    }

    @Override
    public void onDrawerClosed(View arg0) {
	drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED,
		Gravity.RIGHT);
    }

    @Override
    public void onDrawerOpened(View drawerView) {
	// 如果打开的是左边的菜单
	if (drawerView.getTag().equals("LEFT")) {
	    if (kaibesApplication.getUserPhotoBitmap() != null) {
		((RoundImageView) drawerView
			.findViewById(R.id.mainMenuLeftPhoto))
			.setImageBitmap(kaibesApplication.getUserPhotoBitmap());
	    }
	    if (kaibesApplication.getUserInformationData() != null) {
		((TextView) drawerView
			.findViewById(R.id.mainMenuLeftUsernickname))
			.setText(kaibesApplication.getUserInformationData()
				.getNickname());
		((TextView) drawerView
			.findViewById(R.id.mainMenuLeftUsersignature))
			.setHint(kaibesApplication.getUserInformationData()
				.getSignature());
	    }
	}
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
	// 内容界面
	View mainLayout = drawerLayout.findViewById(R.id.container);
	// 缩变比例
	float scale = 1 - slideOffset;
	// 右边，即时内容界面大小变化的比例，当slideOffset为0时，rightScale为1，即是内容界面大小不变，当slideOffset为1，rightScale为0.8，这是内容界面的最小比例
	float rightScale = 0.8f + scale * 0.2f;
	// 左边，即是吃菜单界面大小的变化，它的变化和内容界面相反
	float drawerViewScale = 1 - 0.3f * scale;

	// 对drawerView菜单界面大小进行缩放
	ViewHelper.setScaleX(drawerView, drawerViewScale);
	ViewHelper.setScaleY(drawerView, drawerViewScale);

	// 如果这是左菜单
	if (drawerView.getTag().equals("LEFT")) {
	    // 内容界面向右边移动，移动X坐标
	    ViewHelper.setTranslationX(mainLayout,
		    drawerView.getMeasuredWidth() * slideOffset);

	    // 对mainLayout，内容界面进行缩放
	    ViewHelper.setScaleX(mainLayout, rightScale);
	    ViewHelper.setScaleY(mainLayout, rightScale);

	    // 调整位置
	    ViewHelper.setPivotX(mainLayout, 0);
	    ViewHelper
		    .setPivotY(mainLayout, mainLayout.getMeasuredHeight() / 2);

	} else {
	    // 如果这是右边菜单
	    // 内容界面就向左边移动，移动X坐标，向左就是负值
	    ViewHelper.setTranslationX(mainLayout,
		    -drawerView.getMeasuredWidth() * slideOffset);

	    // 对mainLayout内容界面进行缩放
	    ViewHelper.setScaleX(mainLayout, rightScale);
	    ViewHelper.setScaleY(mainLayout, rightScale);

	    // 调整位置
	    ViewHelper.setPivotX(mainLayout, mainLayout.getMeasuredWidth());
	    ViewHelper
		    .setPivotY(mainLayout, mainLayout.getMeasuredHeight() / 2);
	}

	mainLayout.invalidate();
    }

    @Override
    public void onDrawerStateChanged(int arg0) {
	// TODO
	// switch (arg0)
	// {
	// case DrawerLayout.STATE_DRAGGING:
	// Log.i(TAG,"拖动状态");
	// break;
	// case DrawerLayout.STATE_IDLE:
	// Log.i(TAG,"静止状态");
	// break;
	// case DrawerLayout.STATE_SETTLING:
	// Log.i(TAG,"设置状态");
	// break;
	// default:
	// break;
	// }
    }

}
