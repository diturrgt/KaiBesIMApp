package com.kaibes.im.model.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kaibes.im.R;
import com.kaibes.im.control.activity.KaibesActivity;
import com.kaibes.im.utils.StaticValuesUtil;

public class ClassicalGridviewAdapter extends KaibesAdapter<String> {

    public ClassicalGridviewAdapter(KaibesActivity kaibesActivity,
	    ArrayList<String> datas) {
	super(kaibesActivity, datas);
	// TODO Auto-generated constructor stub
    }

    @Override
    public long getItemId(int position) {
	return StaticValuesUtil.FACE_HACE_CLASSIC.get(getDatas().get(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	ViewHolder holder = null;
	if (convertView == null) {
	    convertView = LayoutInflater.from(getKaibesActivity()).inflate(
		    R.layout.gridview_item, parent, false);
	    holder = new ViewHolder();
	    holder.imageView = (ImageView) convertView
		    .findViewById(R.id.iv_gridview_item);
	    convertView.setTag(holder);
	} else {
	    holder = (ViewHolder) convertView.getTag();
	}

	holder.imageView.setImageResource((int) getItemId(position));
	return convertView;
    }

    private static class ViewHolder {
	public ImageView imageView;
    }

}
