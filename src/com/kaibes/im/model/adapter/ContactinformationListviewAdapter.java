package com.kaibes.im.model.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kaibes.im.R;
import com.kaibes.im.control.activity.KaibesActivity;
import com.kaibes.im.model.data.ContactListviewData;

public class ContactinformationListviewAdapter extends
	KaibesAdapter<ContactListviewData> {

    public ContactinformationListviewAdapter(KaibesActivity kaibesActivity,
	    ArrayList<ContactListviewData> datas) {
	super(kaibesActivity, datas);
	// TODO Auto-generated constructor stub
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	ViewHolder holder = null;
	if (convertView == null) {
	    convertView = LayoutInflater.from(getKaibesActivity()).inflate(
		    R.layout.list_item_contactinformation, parent, false);
	    holder = new ViewHolder();
	    holder.name = ((TextView) convertView.findViewById(R.id.tvContactinformationName));
	    holder.content = ((TextView) convertView.findViewById(R.id.tvContactinformationContent));
	    convertView.setTag(holder);
	} else {
	    holder = (ViewHolder) convertView.getTag();
	}
	holder.name.setText(getItem(position).getName());
	holder.content.setText(getItem(position).getContent());
	return convertView;
    }
    
    private static class ViewHolder{
	public TextView name;
	public TextView content;
    }

}
