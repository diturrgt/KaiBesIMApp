package com.kaibes.im.model.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kaibes.im.R;
import com.kaibes.im.control.activity.KaibesActivity;
import com.kaibes.im.model.data.ContactInformationData;
import com.kaibes.im.utils.FaceUtil;
import com.kaibes.im.view.RoundImageView;

public class ConversationListviewAdapter extends KaibesAdapter<String> {

    public ConversationListviewAdapter(KaibesActivity kaibesActivity,
	    ArrayList<String> conversations) {
	super(kaibesActivity, conversations);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	ViewHolder holder = null;

	if (convertView == null) {
	    convertView = LayoutInflater.from(getKaibesActivity()).inflate(
		    R.layout.list_item_conversation, parent, false);
	    holder = new ViewHolder();
	    holder.remark = (TextView) convertView
		    .findViewById(R.id.conversationNickname);
	    holder.message = ((TextView) convertView
		    .findViewById(R.id.conversationMessage));
	    holder.photoview = (RoundImageView) convertView
		    .findViewById(R.id.coversationPhoto);
	    convertView.setTag(holder);
	} else {
	    holder = (ViewHolder) convertView.getTag();
	}

	if (getDatas() == null || getDatas().size() == 0) {
	} else {
	    ContactInformationData contactInformationData = getKaibesActivity()
		    .getKaibesApplication().getContactInformationData(
			    getItem(position));

	    if (contactInformationData.getRemark().equals("")) {
		holder.remark.setText(contactInformationData.getNickname());
	    } else {
		holder.remark.setText(contactInformationData.getRemark());
	    }

	    if (contactInformationData.getMessage() == null) {
		holder.message.setText("["
			+ getKaibesActivity().getString(R.string.signature)
			+ "]" + contactInformationData.getSignature());
	    } else {
		holder.message.setText(FaceUtil.showMessageFace(
			getKaibesActivity(),
			contactInformationData.getMessage(), 0.6f));
	    }

	    getKaibesActivity().getKaibesApplication()
		    .setContactPhotoImageOrGray(holder.photoview,
			    contactInformationData);
	}
	return convertView;
    }

    private static class ViewHolder {
	public TextView remark;
	public TextView message;
	public RoundImageView photoview;
    }
}
