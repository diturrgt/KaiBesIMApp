package com.kaibes.im.model.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kaibes.im.R;
import com.kaibes.im.control.activity.KaibesActivity;
import com.kaibes.im.model.data.ContactInformationData;
import com.kaibes.im.view.RoundImageView;

public class ContactListviewAdapter extends KaibesAdapter<String> {

    public ContactListviewAdapter(KaibesActivity kaibesActivity,
	    ArrayList<String> contactDatas) {
	super(kaibesActivity, contactDatas);
	// TODO Auto-generated constructor stub
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	ViewHolder holder = null;

	if (convertView == null) {
	    convertView = LayoutInflater.from(getKaibesActivity()).inflate(
		    R.layout.list_item_contact, parent, false);
	    holder = new ViewHolder();
	    holder.remark = (TextView) convertView
		    .findViewById(R.id.contactNickname);
	    holder.signature = (TextView) convertView
		    .findViewById(R.id.contactSignature);
	    convertView.setTag(holder);
	} else {
	    holder = (ViewHolder) convertView.getTag();
	}

	ContactInformationData contactInformationData = getKaibesActivity()
		.getKaibesApplication().getContactInformationData(
			getItem(position));

	getKaibesActivity().getKaibesApplication().setContactPhotoImageOrGray(
		(RoundImageView) convertView.findViewById(R.id.contactPhoto),
		contactInformationData);

	if (contactInformationData.getRemark().equals("")) {
	    holder.remark.setText(contactInformationData.getNickname());
	} else {
	    holder.remark.setText(contactInformationData.getRemark());
	}

	if (contactInformationData.isOnline()) {
	    holder.signature.setText("["
		    + getKaibesActivity().getString(R.string.online) + "]"
		    + contactInformationData.getSignature());
	} else {
	    holder.signature.setText("["
		    + getKaibesActivity().getString(R.string.offline) + "]"
		    + contactInformationData.getSignature());
	}

	return convertView;
    }

    private static class ViewHolder {
	public TextView remark;
	public TextView signature;
    }

}
