package com.kaibes.im.model.adapter;

import java.util.ArrayList;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.kaibes.im.control.activity.KaibesActivity;

public abstract class KaibesAdapter<E> extends BaseAdapter {

    private ArrayList<E> datas = null;
    private KaibesActivity kaibesActivity;

    public KaibesAdapter(KaibesActivity kaibesActivity, ArrayList<E> datas) {
	setKaibesActivity(kaibesActivity);
	setDatas(datas);
    }

    @Override
    public int getCount() {
	return getDatas().size();
    }

    @Override
    public E getItem(int position) {
	return getDatas().get(position);
    }

    @Override
    public long getItemId(int position) {
	return position;
    }

    @Override
    public abstract View getView(int position, View convertView,
	    ViewGroup parent);

    // ---------------------------GET AND SET 1--------------------------
    public ArrayList<E> getDatas() {
	return datas;
    }

    public void setDatas(ArrayList<E> datas) {
	this.datas = datas;
    }

    public KaibesActivity getKaibesActivity() {
	return kaibesActivity;
    }

    private void setKaibesActivity(KaibesActivity kaibesActivity) {
	this.kaibesActivity = kaibesActivity;
    }

}
