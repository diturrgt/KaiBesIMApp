package com.kaibes.im.model.adapter;

import java.util.ArrayList;

import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kaibes.im.R;
import com.kaibes.im.control.activity.KaibesActivity;
import com.kaibes.im.model.data.ChatListviewData;
import com.kaibes.im.utils.FaceUtil;
import com.kaibes.im.view.RoundImageView;
import com.kaibes.im.view.fragment.ContactInformationFragment;
import com.kaibes.im.view.fragment.UserInformationFragment;

public class ChatListviewAdapter extends KaibesAdapter<ChatListviewData>
	implements OnClickListener {

    public ChatListviewAdapter(KaibesActivity kaibesActivity,
	    ArrayList<ChatListviewData> datas) {
	super(kaibesActivity, datas);
	// TODO Auto-generated constructor stub
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	ViewHolder holder = null;
	if (convertView == null) {
	    convertView = LayoutInflater.from(getKaibesActivity()).inflate(
		    R.layout.list_item_chat, parent, false);
	    holder = new ViewHolder();
	    holder.textView = (TextView) convertView
		    .findViewById(R.id.tvChatMessage);
	    holder.contact = (RoundImageView) convertView
		    .findViewById(R.id.ivChatPhotoContact);
	    holder.self = (RoundImageView) convertView
		    .findViewById(R.id.ivChatPhotoSelf);
	    convertView.setTag(holder);
	} else {
	    holder = (ViewHolder) convertView.getTag();
	}

	holder.textView.setOnClickListener(this);

	if (getItem(position).getContactInformationData() != null) {
	    holder.contact.setVisibility(View.VISIBLE);
	    holder.self.setVisibility(View.GONE);
	    holder.textView
		    .setBackgroundResource(R.drawable.selector_textview_friendbubble);
	    ((LinearLayout) convertView.findViewById(R.id.llChatMessage))
		    .setGravity(Gravity.LEFT);
	    getKaibesActivity().getKaibesApplication()
		    .setContactPhotoImageOrGray(holder.contact,
			    getItem(position).getContactInformationData());
	    holder.contact.setOnClickListener(this);
	} else {
	    holder.contact.setVisibility(View.GONE);
	    holder.self.setVisibility(View.VISIBLE);
	    holder.textView
		    .setBackgroundResource(R.drawable.selector_textview_userbubble);
	    ((LinearLayout) convertView.findViewById(R.id.llChatMessage))
		    .setGravity(Gravity.RIGHT);
	    if (getKaibesActivity().getKaibesApplication().getUserPhotoPath()
		    .exists()) {
		holder.self.setImageBitmap(BitmapFactory
			.decodeFile(getKaibesActivity().getKaibesApplication()
				.getUserPhotoPath().getPath()));
	    }
	    holder.self.setOnClickListener(this);

	}

	holder.textView.setText("");
	holder.textView.setText(FaceUtil.showMessageFace(getKaibesActivity(),
		getItem(position).getMessage()));

	return convertView;
    }

    @Override
    public void onClick(View v) {
	switch (v.getId()) {
	case R.id.tvChatMessage:
	    // TODO
	    break;

	case R.id.ivChatPhotoContact:
	    getKaibesActivity()
		    .changeFragment(ContactInformationFragment.class);
	    break;

	case R.id.ivChatPhotoSelf:
	    getKaibesActivity().changeFragment(UserInformationFragment.class);
	    break;

	default:
	    break;
	}
    }

    private static class ViewHolder {
	public TextView textView;
	public RoundImageView contact;
	public RoundImageView self;
    }

}
