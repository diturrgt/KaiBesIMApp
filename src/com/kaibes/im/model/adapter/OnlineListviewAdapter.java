package com.kaibes.im.model.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kaibes.im.R;
import com.kaibes.im.control.activity.KaibesActivity;

public class OnlineListviewAdapter extends KaibesAdapter<String> {

    public OnlineListviewAdapter(KaibesActivity kaibesActivity,
	    ArrayList<String> contactDatas) {
	super(kaibesActivity, contactDatas);
	// TODO Auto-generated constructor stub
    }

    public OnlineListviewAdapter(KaibesActivity kaibesActivity) {
	super(kaibesActivity, kaibesActivity.getKaibesApplication().allOnline);
	// TODO Auto-generated constructor stub
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	ViewHolder holder = null;

	if (convertView == null) {
	    convertView = LayoutInflater.from(getKaibesActivity()).inflate(
		    R.layout.list_item_testresult, parent, false);
	    holder = new ViewHolder();
	    holder.textView = (TextView) convertView
		    .findViewById(R.id.testContactname);
	    convertView.setTag(holder);
	} else {
	    holder = (ViewHolder) convertView.getTag();
	}

	holder.textView.setText(getItem(position));
	return convertView;
    }

    private static class ViewHolder {
	public TextView textView;
    }

}
