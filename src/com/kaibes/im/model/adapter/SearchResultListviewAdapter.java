package com.kaibes.im.model.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kaibes.im.R;
import com.kaibes.im.control.activity.KaibesActivity;
import com.kaibes.im.model.data.ContactInformationData;
import com.kaibes.im.view.RoundImageView;

public class SearchResultListviewAdapter extends
	KaibesAdapter<ContactInformationData> {

    public SearchResultListviewAdapter(KaibesActivity kaibesActivity,
	    ArrayList<ContactInformationData> contactDatas) {
	super(kaibesActivity, contactDatas);
	// TODO Auto-generated constructor stub
    }

    public SearchResultListviewAdapter(KaibesActivity kaibesActivity) {
	super(kaibesActivity, kaibesActivity.getKaibesApplication()
		.getSearchDatas());
	// TODO Auto-generated constructor stub
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	ViewHolder holder = null;

	if (convertView == null) {
	    convertView = LayoutInflater.from(getKaibesActivity()).inflate(
		    R.layout.list_item_searchresult, parent, false);
	    holder = new ViewHolder();
	    holder.photoView = (RoundImageView) convertView
		    .findViewById(R.id.searchPhoto);
	    holder.remark = (TextView) convertView
		    .findViewById(R.id.searchNickname);
	    holder.signature = (TextView) convertView
		    .findViewById(R.id.searchSignature);
	    convertView.setTag(holder);
	} else {
	    holder = (ViewHolder) convertView.getTag();
	}

	getKaibesActivity().getKaibesApplication().setSearchPhotoImageOrGray(
		holder.photoView, getItem(position));

	if (getItem(position).getRemark().equals("")) {
	    holder.remark.setText(getItem(position).getNickname());
	} else {
	    holder.remark.setText(getItem(position).getRemark());
	}

	if (getItem(position).isOnline()) {
	    holder.signature.setText("[在线]" + getItem(position).getSignature());
	} else {
	    holder.signature.setText("[离线]" + getItem(position).getSignature());
	}

	return convertView;
    }

    private static class ViewHolder {
	public RoundImageView photoView;
	public TextView remark;
	public TextView signature;
    }

}
