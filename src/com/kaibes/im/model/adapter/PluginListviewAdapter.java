package com.kaibes.im.model.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kaibes.im.R;
import com.kaibes.im.control.activity.KaibesActivity;
import com.kaibes.im.model.data.PluginListviewData;

public class PluginListviewAdapter extends KaibesAdapter<PluginListviewData> {

    public PluginListviewAdapter(KaibesActivity kaibesActivity) {
	super(kaibesActivity, kaibesActivity.getKaibesApplication()
		.getPluginListviewDatas());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	ViewHolder holder = null;

	if (convertView == null) {
	    convertView = LayoutInflater.from(getKaibesActivity()).inflate(
		    R.layout.list_item_plugin, parent, false);
	    holder = new ViewHolder();
	    holder.name = (TextView) convertView
		    .findViewById(R.id.pluginContactname);
	    holder.notification = (TextView) convertView
		    .findViewById(R.id.pluginNotification);
	    convertView.setTag(holder);
	} else {
	    holder = (ViewHolder) convertView.getTag();
	}

	holder.name.setText(getItem(position).getContactname());
	holder.notification.setText(getItem(position).getMessage());

	return convertView;
    }

    private static class ViewHolder {
	public TextView name;
	public TextView notification;
    }

}
