package com.kaibes.im.utils;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class DomEngineUtil {

    private static DocumentBuilderFactory documentBuilderFactory;
    private static DocumentBuilder documentBuilder;
    private static TransformerFactory transformerFactory;
    private static Transformer transformer;

    public DomEngineUtil() {
	System.err.println("hello DOM !");
    }

    static {
	documentBuilderFactory = DocumentBuilderFactory.newInstance();
	try {
	    documentBuilder = documentBuilderFactory.newDocumentBuilder();
	} catch (ParserConfigurationException e) {
	    e.printStackTrace();
	}

	transformerFactory = TransformerFactory.newInstance();
	try {
	    transformer = transformerFactory.newTransformer();
	} catch (TransformerConfigurationException e) {
	    e.printStackTrace();
	}
	transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
	transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
    }

    public static void createNewXmlFile(File file, String grounpname) {

	Document document = documentBuilder.newDocument();
	Element root = document.createElement("root");
	root.setAttribute("name", grounpname);
	document.appendChild(root);
	DOMSource source = new DOMSource(document);

	try {
	    StreamResult streamResult = new StreamResult(file);
	    transformer.transform(source, streamResult);
	} catch (TransformerException e) {
	    e.printStackTrace();
	}

    }

    public static Transaction newTransactionInstance(File file) {
	return new Transaction(file);
    }

    public static class Transaction {
	private Document document;
	private DOMSource source;
	private StreamResult streamResult;
	private Element root;

	private Transaction(File file) {
	    streamResult = new StreamResult(file);
	    try {
		document = documentBuilder.parse(file);
		source = new DOMSource(document);
		root = document.getDocumentElement();
	    } catch (SAXException | IOException e) {
		e.printStackTrace();
	    }
	}

	// public void addNewMessage(Account account) {
	// Element message = document.createElement("message");
	//
	// for (int i = 0; i < account.getSize(); i++) {
	// String key = account.getKey(i);
	// String value = account.getValues(key);
	//
	// Element element = document.createElement(key);
	// element.setTextContent(value);
	// message.appendChild(element);
	//
	// }
	// root.appendChild(message);
	// }

	public void deleteMessage(String id) {
	    Element message = (Element) root.getElementsByTagName(id);
	    root.removeChild(message);
	}

	// public ArrayList<Account> readMeassage() {
	// ArrayList<Account> accounts = new ArrayList<Account>();
	// NodeList list = root.getElementsByTagName("message");
	// for (int i = 0; i < list.getLength(); i++) {
	// Element message = (Element) list.item(i);
	// Account account = new Account();
	// for (int j = 0; j < account.getSize(); j++) {
	// String key = account.getKey(j);
	// account.setValue(key, message.getElementsByTagName(key)
	// .item(0).getTextContent());
	// }
	// accounts.add(account);
	// }
	//
	// return accounts;
	// }

	public void commit() {
	    try {
		transformer.transform(source, streamResult);
	    } catch (TransformerException e) {
		e.printStackTrace();
	    }
	}

	public void setName(String name) {
	    root.setAttribute("name", name);
	}

	public String getName() {
	    return root.getAttribute("name");
	}
    }
}
