package com.kaibes.im.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import com.kaibes.im.R;

public class SoundPoolUtil {

    private SoundPool soundPool = null;
    private int[] soundIds = null;

    @SuppressWarnings("deprecation")
    @SuppressLint({ "NewApi" })
    public SoundPoolUtil(Context context) {
	// AudioAttributes.Builder aaBuilder = new AudioAttributes.Builder();
	// aaBuilder.setContentType(AudioAttributes.CONTENT_TYPE_MUSIC);
	// aaBuilder.setFlags(AudioAttributes.FLAG_HW_AV_SYNC);
	// aaBuilder.setLegacyStreamType(AudioManager.STREAM_NOTIFICATION);
	// aaBuilder.setUsage(AudioAttributes.USAGE_NOTIFICATION);
	//
	// SoundPool.Builder spBuilder = new SoundPool.Builder();
	// spBuilder.setMaxStreams(20);
	// spBuilder.setAudioAttributes(aaBuilder.build());
	//
	// soundPool = spBuilder.build();
	soundPool = new SoundPool(20, AudioManager.STREAM_SYSTEM, 5);
	soundIds = new int[17];

	soundIds[0] = soundPool.load(context, R.raw.fu, 1);
	soundIds[1] = soundPool.load(context, R.raw.fv, 1);
	soundIds[2] = soundPool.load(context, R.raw.fw, 1);
	soundIds[3] = soundPool.load(context, R.raw.fx, 1);
	soundIds[4] = soundPool.load(context, R.raw.fy, 1);
	soundIds[5] = soundPool.load(context, R.raw.gc, 1);
	soundIds[6] = soundPool.load(context, R.raw.gd, 1);
	soundIds[7] = soundPool.load(context, R.raw.ge, 1);
	soundIds[8] = soundPool.load(context, R.raw.gf, 1);
	soundIds[9] = soundPool.load(context, R.raw.gg, 1);
	soundIds[10] = soundPool.load(context, R.raw.gh, 1);
	soundIds[11] = soundPool.load(context, R.raw.gi, 1);
	soundIds[12] = soundPool.load(context, R.raw.gj, 1);
	soundIds[13] = soundPool.load(context, R.raw.gk, 1);
	soundIds[14] = soundPool.load(context, R.raw.gl, 1);
	soundIds[15] = soundPool.load(context, R.raw.gm, 1);
	soundIds[16] = soundPool.load(context, R.raw.online, 1);
    }

    public void play(int i) {
	soundPool.play(soundIds[i], 1, 1, 0, 0, 1);
    }
}
