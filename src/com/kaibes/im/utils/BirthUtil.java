package com.kaibes.im.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class BirthUtil {

    private static final String[] zodiacs = { "猴", "鸡", "狗", "猪", "鼠", "牛",
	    "虎", "兔", "龙", "蛇", "马", "羊" };
    private static final String[] constellations = { "水瓶座", "双鱼座", "牡羊座",
	    "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "魔蝎座" };
    private static final int[] constellationEdgeDay = { 20, 19, 21, 21, 21, 22,
	    23, 23, 23, 23, 22, 22 };

    public static int getAge(String birth) {
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd",
		Locale.CHINA);

	Date birthDate = null;
	try {
	    birthDate = simpleDateFormat.parse(birth);
	} catch (ParseException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	int day = (int) Math.floor((new Date().getTime() - birthDate.getTime())
		/ (24 * 60 * 60 * 1000 * 365f));

	return day;
    }

    public static String getZodica(String birth) {
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd",
		Locale.CHINA);

	Date birthDate = null;
	try {
	    birthDate = simpleDateFormat.parse(birth);
	} catch (ParseException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	Calendar calendar = Calendar.getInstance(Locale.CHINA);
	calendar.setTime(birthDate);

	return zodiacs[calendar.get(Calendar.YEAR) % 12];
    }

    public static String getConstellation(String birth) {

	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd",
		Locale.CHINA);

	Date birthDate = null;
	try {
	    birthDate = simpleDateFormat.parse(birth);
	} catch (ParseException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	Calendar calendar = Calendar.getInstance(Locale.CHINA);
	calendar.setTime(birthDate);

	int month = calendar.get(Calendar.MONTH);
	int day = calendar.get(Calendar.DAY_OF_MONTH);

	if (day < constellationEdgeDay[month]) {
	    month = month - 1;
	}

	if (month >= 0) {
	    return constellations[month];
	}

	return constellations[11];
    }
}
