package com.kaibes.im.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.util.Base64;

public class ByteStringUtil {

    public static String bytes2StringBase64(byte[] bs) {
	return Base64.encodeToString(bs, Base64.DEFAULT);
    }

    public static byte[] string2BytesBase64(String content) {
	return Base64.decode(content, Base64.DEFAULT);
    }

    public static String file2StringBase64(String path) {
	try {
	    FileInputStream fis = new FileInputStream(path);
	    byte[] bs = new byte[fis.available()];
	    fis.read(bs);
	    fis.close();
	    return Base64.encodeToString(bs, Base64.DEFAULT);
	} catch (FileNotFoundException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return null;
    }

    public static void string2FileBase64(String content, String path) {
	try {
	    FileOutputStream fos = new FileOutputStream(path, true);
	    byte[] bs = Base64.decode(content, Base64.DEFAULT);
	    fos.write(bs);
	    fos.flush();
	    fos.close();
	} catch (FileNotFoundException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public static void stringWrite2File(String content, String path) {
	try {
	    FileOutputStream fos = new FileOutputStream(path, true);
	    fos.write(content.getBytes());
	    fos.flush();
	    fos.close();
	} catch (FileNotFoundException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public static void string2FosBase64(String content, FileOutputStream fos) {
	try {
	    byte[] bs = Base64.decode(content, Base64.DEFAULT);
	    fos.write(bs);
	    fos.flush();
	} catch (FileNotFoundException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

}
