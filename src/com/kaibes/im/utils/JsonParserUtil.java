package com.kaibes.im.utils;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParserUtil {

    private JSONObject jsonObject;
    private JSONObject jsonObjectSon;
    private JSONArray jsonArraySon;

    private int index = 0;

    public JsonParserUtil(String json) {
	try {
	    jsonObject = new JSONObject(json);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public JsonParserUtil() {
	jsonObject = new JSONObject();
    }

    public JsonParserUtil(String filename, String data) {
	jsonObject = new JSONObject();
	putValue("h", 0).putValue("a", filename).putValue("d", data);
    }

    public JsonParserUtil newSelf() {
	jsonObject = new JSONObject();
	return this;
    }

    public JsonParserUtil newJsonObjectSon() {
	jsonObjectSon = new JSONObject();
	return this;
    }

    public JsonParserUtil newJsonArraySon() {
	jsonArraySon = new JSONArray();
	return this;
    }

    public JsonParserUtil putValue(String name, Object value) {
	try {
	    jsonObject.put(name, value);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return this;
    }

    public JsonParserUtil putJsonObjectSonValue(String name, Object value) {
	try {
	    jsonObjectSon.put(name, value);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return this;
    }

    public JsonParserUtil putJsonArraySonValue(Object value) {
	jsonArraySon.put(value);
	return this;
    }

    public JsonParserUtil parserJsonString(String json) {
	try {
	    jsonObject = new JSONObject(json);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return this;
    }

    public JsonParserUtil addJSONObjectSon() {
	jsonArraySon.put(jsonObjectSon);
	return this;
    }

    public JsonParserUtil addJsonArraySon(String name) {
	try {
	    jsonObject.put(name, jsonArraySon);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return this;
    }

    public Object getValue(String name) {
	try {
	    return jsonObject.get(name);
	} catch (JSONException e) {
	    return null;
	}
    }

    public JsonParserUtil setJsonArraySon(String name) {
	try {
	    jsonArraySon = jsonObject.getJSONArray(name);
	    if (jsonArraySon.length() > 0) {
		setJsonObjectSon(0);
		return this;
	    }
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public JSONArray getJsonArraySon() {
	return jsonArraySon;
    }

    public JsonParserUtil setJsonObjectSon(int index) {
	try {
	    // jsonObjectSon = (JSONObject) jsonArraySon.get(index);
	    jsonObjectSon = jsonArraySon.getJSONObject(index);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return this;
    }

    public boolean nextJsonObjectSon() {
	if (index < jsonArraySon.length()) {
	    try {
		jsonObjectSon = jsonArraySon.getJSONObject(index++);
	    } catch (JSONException e) {
		e.printStackTrace();
	    }
	    return true;
	} else {
	    index = 0;
	    return false;
	}
    }

    public JSONObject getJsonObjectSon() {
	return jsonObjectSon;
    }

    public Object getJsonObjectSonValue(String name) {
	try {
	    return jsonObjectSon.get(name);
	} catch (JSONException e) {
	    return null;
	}
    }

    public String toString() {
	return jsonObject.toString();
    }

    @SuppressWarnings("unchecked")
    public <E> ArrayList<E> jsonArray2arrayList() {

	ArrayList<E> array = new ArrayList<E>();

	try {
	    for (int i = 0; i < getJsonArraySon().length(); i++) {
		array.add((E) getJsonArraySon().get(i));
	    }
	} catch (JSONException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	return array;
    }

}
