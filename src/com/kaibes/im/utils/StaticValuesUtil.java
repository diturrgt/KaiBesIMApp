package com.kaibes.im.utils;

import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;

import com.kaibes.im.R;

public class StaticValuesUtil {

    public static final String MAN = "男";
    public static final String WOMAN = "女";

    public static final String FILES = "files";
    public static final String CACHE = "cache";

    public static final String PHOTO_PATH = File.separator + "photo"
	    + File.separator;
    public static final String TMP_PHOTO = "temp_photo.jpg";

    public static final String USER = "user";
    public static final String CONTACT = "contact";
    public static final String SEARCH = "search";

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    public static final String USER_PHOTO = "user_photo";
    public static final String CONTACT_PHOTO = "contact_photo";
    public static final String SEARCH_PHOTO = "search_photo";

    public static final String PHOTO = "photo";
    public static final String PHOTO_DEAFULT = "photo_default.jpg";

    public static final String FACE_HASH_DELETE_FACE_NAME = "face_delete";
    public static final HashMap<String, Integer> FACE_HACE_CLASSIC = new HashMap<String, Integer>();
    public static final String[] faceHashClassicKeys = new String[] {
	    "face_qq_0", "face_qq_1", "face_qq_2", "face_qq_3", "face_qq_4",
	    "face_qq_5", "face_qq_6", "face_qq_7", "face_qq_8", "face_qq_9",
	    "face_qq_10", "face_qq_11", "face_qq_12", "face_qq_13",
	    "face_qq_14", "face_qq_15", "face_qq_16", "face_qq_17",
	    "face_qq_18", "face_qq_19", "face_qq_20", "face_qq_21",
	    "face_qq_22", "face_qq_23", "face_qq_24", "face_qq_25",
	    "face_qq_26", "face_qq_27", "face_qq_28", "face_qq_29",
	    "face_qq_30", "face_qq_31", "face_qq_32", "face_qq_33",
	    "face_qq_34", "face_qq_35", "face_qq_36", "face_qq_37",
	    "face_qq_38", "face_qq_39", "face_qq_40", "face_qq_41",
	    "face_qq_42", "face_qq_43", "face_qq_44", "face_qq_45",
	    "face_qq_46", "face_qq_47", "face_qq_48", "face_qq_49",
	    "face_qq_50", "face_qq_51", "face_qq_52", "face_qq_53",
	    "face_qq_54", "face_qq_55", "face_qq_56", "face_qq_57",
	    "face_qq_58", "face_qq_59", "face_qq_60", "face_qq_61",
	    "face_qq_62", "face_qq_63", "face_qq_64", "face_qq_65",
	    "face_qq_66", "face_qq_67", "face_qq_68", "face_qq_69",
	    "face_qq_70", "face_qq_71", "face_qq_72", "face_qq_73",
	    "face_qq_74", "face_qq_75", "face_qq_76", "face_qq_77",
	    "face_qq_78", "face_qq_79" };

    static {
	try {
	    for (int i = 0; i < faceHashClassicKeys.length; i++) {
		Field field = R.drawable.class.getDeclaredField("face_qq_" + i);
		FACE_HACE_CLASSIC.put(faceHashClassicKeys[i],
			field.getInt(null));
	    }
	    Field field = R.drawable.class
		    .getDeclaredField(FACE_HASH_DELETE_FACE_NAME);
	    FACE_HACE_CLASSIC.put(FACE_HASH_DELETE_FACE_NAME,
		    field.getInt(null));
	} catch (NoSuchFieldException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IllegalAccessException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IllegalArgumentException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

}
