package com.kaibes.im.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class KaibesDataHelper extends DatabaseHelper {

    private final static int DB_VERSION = 1;

    public KaibesDataHelper(Context context, String name) {
	super(context, name, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
	db.execSQL("create table contactsinformation("
		+ "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
		+ "contactname VARCHAR(16) UNIQUE NOT NULL DEFAULT '', "
		+ "nickname VARCHAR(16) NOT NULL DEFAULT '', "
		+ "photo VARCHAR(32) NOT NULL DEFAULT '', "
		+ "sex VARCHAR(4) NOT NULL DEFAULT '', "
		+ "birth VARCHAR(10) NOT NULL DEFAULT '', "
		+ "signature TEXT NOT NULL DEFAULT '', "
		+ "introduction  TEXT NOT NULL DEFAULT '', "
		+ "job VARCHAR(16) NOT NULL DEFAULT '', "
		+ "company VARCHAR(16) NOT NULL DEFAULT '', "
		+ "school VARCHAR(16) NOT NULL DEFAULT '', "
		+ "address VARCHAR(16) NOT NULL DEFAULT '', "
		+ "hometown VARCHAR(16) NOT NULL DEFAULT '', "
		+ "email TEXT NOT NULL DEFAULT '', "
		+ "remark TEXT NOT NULL DEFAULT ''" + ")");

    }

    public void replaceContactinformation(String[] values) {
	replaceContactinformation(
		new String[] { "contactname", "nickname", "photo", "sex",
			"birth", "signature", "introduction", "job", "company",
			"school", "address", "hometown", "email", "remark" },
		values);
    }

    private void replaceContactinformation(String[] keys, String[] values) {
	ContentValues contentValues = new ContentValues();

	for (int i = 0; i < values.length; i++) {
	    contentValues.put(keys[i], values[i]);
	}

	replace("contactsinformation", null, contentValues);
    }

    public Cursor getFriendInformationCursor(String selection,
	    String[] selectionArgs) {
	return getCursor("contactsinformation", new String[] { "_id",
		"contactname", "nickname", "photo", "sex", "birth",
		"signature", "introduction", "job", "company", "school",
		"address", "hometown", "email", "remark" }, selection,
		selectionArgs, null, null, "_id desc", null);
    }

    public Cursor getFriendInformationCursor() {
	return getFriendInformationCursor(null, null);
    }

}
