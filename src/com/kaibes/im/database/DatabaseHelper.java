package com.kaibes.im.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context, String name, CursorFactory factory,
	    int version, DatabaseErrorHandler errorHandler) {
	super(context, name, factory, version, errorHandler);
	// TODO Auto-generated constructor stub
    }

    public DatabaseHelper(Context context, String name, CursorFactory factory,
	    int version) {
	super(context, name, factory, version);
	// TODO Auto-generated constructor stub
    }

    @Override
    public abstract void onCreate(SQLiteDatabase db);

    @Override
    public void onOpen(SQLiteDatabase db) {
	// TODO Auto-generated method stub
	super.onOpen(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	// TODO Auto-generated method stub
    }

    public void deleteContactinformation(String contactname) {
	delete("contactsinformation", "contactname = ?",
		new String[] { contactname });
    }

    protected void replace(String table, String nullColumnHack,
	    ContentValues values) {
	SQLiteDatabase writer = getWritableDatabase();
	writer.replace(table, nullColumnHack, values);
	writer.close();
    }

    protected void delete(String table, String whereClause, String[] whereArgs) {
	SQLiteDatabase writer = getWritableDatabase();
	writer.delete(table, whereClause, whereArgs);
    }

    protected Cursor getCursor(String table, String[] columns,
	    String selection, String[] selectionArgs, String groupBy,
	    String having, String orderBy, String limit) {
	SQLiteDatabase reader = getReadableDatabase();
	Cursor cursor = reader.query(table, columns, selection, selectionArgs,
		groupBy, having, orderBy, limit);
	// reader.close();
	return cursor;
    }

}
